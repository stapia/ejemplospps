
# Programación para Sistemas

## Ejercicios Cadenas Alfanuméricas (String)

### Ejercicio 1

* Escribe un programa que:

  1. Lea **varias** cadenas alfanuméricas con longitud máxima de 7 caracteres
  2. Y las escriba en el canal de salida
  3. Hasta que no se pueda leer (se ha cerrado el canal de entrada).
  4. Busca en la Web cómo se *cierra* el canal de entrada.

  Al comprobar el programa escriba palabras más largas y compruebe
  qué ocurre. 

### Ejercicio 2

* Escribe un archivo de cabeceras (header file) *copia.h* con el 
  siguiente contenido:

  ```C
  extern unsigned modo;

  /* Copia origen en dest teniendo en cuenta que el tamaño
  de dest es size */
  void copiar(char* dest, size_t size, const char *origen);
  ```

  Ponlo en una carpeta *include*

* Escribe un archivo *copia.c* donde:
  
  1. Definas la variable `modo` y la inicialices a `0`.
  2. Escribas la implementación de la función *copia* teniendo
    en cuenta que:
  3. Si el modo es 0 se debe copiar la cadena origen en dest hasta
    alcanzar la longitud máxima asumible en dest. 
  4. Si el modo es distinto de 0 se debe copiar el *final* de la 
    cadena origen (como si se copiase la cadena desde el final
    hacia el principio). 

  **Solo** se pueden utilizar las funciones strcpy y strlen. 
  Más concretamente **no** se puede usar `strncpy`

* Escribe el programa principal en main.c

  1. Incluya copia.h
  2. Declare y defina dos variables de tipo string.
  3. Nota: Naturalmente la segunda más larga que la primera.
  4. Llame a copia primero con modo 0 y luego con modo 1.
  5. Escribendo en el canal de salida el resultado. 
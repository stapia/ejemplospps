
# Programación para Sistemas

## Ejercicios archivos

### Ejercicio 1

* Escribe un programa que:

  1. Genere un número dado por argumento (del programa) de enteros aleatorios entre 1 y 20.
  2. Los guarde en un archivo cuyo nombre se da como segundo argumento del programa.
  3. Muestre mensajes por el canal de errores cuando haya algo que no funcione. 
  4. Y, en esos casos, retorne un código de error (por ejemplo: 2)


### Ejercicio 2

* Escribe un programa que:

  1. Lea un número arbitrario de números enteros desde un archivo de texto que se da como argumento del programa. 
  2. Calcule el máximo y el mínimo de dichos números
  3. Y lo escriba en el canal de salida. 

### Ejercicio 3

* Escribe un programa que:

  1. Lea un número arbitrario de números enteros desde un archivo de texto que se da como argumento del programa. 
  2. Ordene los enteros leídos en orden creciente. Se recomienda usar el algoritmo llamado de la burbuja (bubble sort).
  3. Los escriba ordenados en la salida estándar. 

Opcional, consulta aquí:

https://cplusplus.com/reference/cstdlib/qsort/

La explicación y ejemplo de qsort y trata de aplicarla en este problema. 




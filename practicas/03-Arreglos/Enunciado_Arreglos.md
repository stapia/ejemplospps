
# Programación para Sistemas

## Ejercicios Arrays (Arreglos)

### Ejercicio 1

* Escribe un programa que:
  
  1. Lea de la entrada estándar un número, `N`, que es el tamaño 
    de un array de números enteros positivos. 
  2. Se supone que este número es menor o igual que 5, si
    no fuese así, muestre un mensaje y pidalo de nuevo.
  3. Lea `N` números enteros positivos y los guarde en un array 
    de tamaño máximo 5. 
  4. Calcule y escriba por pantalla: el máximo de los números y
    el número de ellos que son pares

### Ejercicio 2

* Escribe las siguientes funciones, pon su declaración en un 
  archivo, *incrementos.h* y su definicón en *incrementos.c*:

  - Una función, `leer` que lea de la entrada estándar un 
    número entero, `N` y luego `N` números en coma flotante. 
    La función devuelve como resultado `N` y guarda en un parámetro
    *array* los valores leídos. La función recibe como parámetro
    el tamaño máximo del array, si la `N` es mayor devuelve -1 y
    no hace nada. 
  - Una función `escribir` que escribe los valores de un *array*
    de números en coma flotante. 
  - Una función `dif_finitas` que toma como parámetro un *array*,
    calcula la diferencias finitas del mismo y lo da como resultado. 

* Escribe un programa que use las funciones anteriores para 
  calcular las diferencias finitas de segundo grado (aplicar dos 
  veces la función) de una serie de números dados por la entrada
  estándar. Se puede suponer que el programa sólo calcula las 
  diferencias de hasta 10 valores. 
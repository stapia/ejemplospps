
# Programación para Sistemas

## Ejercicios variable dinámica

### Ejercicio 1

* Escribe un programa que:

  1. Tome como parámetro un entero, si no es así debe escribir un mensaje de error en la salida estándar y retornar 1.
  2. Cree una cadena alfanumérica dinámica que guarde exactamente el número de caracteres indicado en el parámetro. El programa debe leer los caracteres **de uno en uno**. 
  3. Muestre la cadena alfanumérica leída marcada dentro de comillas simples.

Para comprobar el programa se recomienda probar a meter caracteres como el espacio, el tabulador o el salto de línea. 

### Ejercicio 2

Escribe un programa que lea desde el canal de entrada estándar varias estructuras tal y como se definen en el ejercicio de estructuras (cantidad física y unidades), una vez leídas todas, se deben escribir en el canal de salida y calcular su media correctamente (es decir, haciendo los cambios de unidades que sean necesarios).

Se puede determinar el número de cantidades físicas a leer por cualquier medio a elección del alumno, pero tiene que ser un tamaño variable y el programa debe utilizar estricmente la memoria necesaria. 

Se recomienda practicar el uso de varios archivos de código fuente, por ejemplo: para declarar las funciones que manejan la estructura, para definirlas, etc. 

Se recomienda usar punteros en todas las funciones que usen la estructura.
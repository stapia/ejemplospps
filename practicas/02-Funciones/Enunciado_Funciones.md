
# Programación para Sistemas

## Ejercicios Funciones

### Ejercicio 1: Función con parámetros por valor (de entrada)

* Escribir una función, `convertir_a_rad`, que convierta el valor
  de un ángulo de grados a radianes. Usa esta función más adelante.

* Escribir una función, `mostrar_z`, que:
  1. Tome como parámetros dos números reales que sean el módulo y 
     el argumento de un número complejo (en grados, no en radianes). 
  2. Escriba en el canal de salida estándar la parte real y la parte 
     imaginaria del número complejo dado por los parámetros 
     en la forma: a + b*i.

* Escribir la función `main` para que:
  1. Llame a la función `mostrar_z` para que se escriban los 
     siguientes números complejos: (1, 45), (2, 30), (2.22, 180), (1, 270),
     (3, 300), siendo el primer número de cada par el módulo del número complejo y el segundo número el argumento en grados. 
  2. Escriba en el canal de salida estándar los números complejos de 
     módulo 1 entre los argumentos 0 y 360 en incrementos de 10 grados. 

* Escribir una nueva función que:    
   1. Tome como parámetro un número en coma flotante
   2. Escriba en el canal estándar los números complejos de modulo 1 para 
      los argumentos entre 0 y 360 con un incremento dado por el parámetro.

* Modificar el programa principal para que:
  1. Escriba los números complejos de modulo 1 en incrementos de 20 grados
  2. Escriba los números complejos de modulo 1 en incrementos de 25 grados

---

### Ejercicio 2: Punteros con pythontutor.com

* Navegar al enlace: [https://pythontutor.com/c.html#mode=edit]

#### Dos punteros y una variable.

* Escribir el siguiente programa:

```
int main() {
  int a;
  int *p1, *p2;
  
  a = 10;
  p1 = &a;
  printf("a es %d, p1 es %p\n", a, p1);
  
  *p1 = 33;
  printf("a es %d, p1 es %p\n", a, p1);

  p2 = p1;
  printf("a es %d, p1 es %p p2 es %p\n", a, p1, p2);
  
  *p2 = 99;
  printf("a es %d, p2 es %p\n", a, p2);

  return 0;
}
```

Explicar el comportamiento del programa. 

#### Dos variables y un puntero.

* Escribir el siguiente programa

```
int main() {
  int a, b;
  int *p;
  
  a = 11;
  b = 99;
  p = &a;
  printf("a = %d, b = %d, p es %p\n", a, b, p);
  
  *p = 33;
  printf("a = %d, b = %d, p es %p\n", a, b, p);

  p = &b;
  printf("a = %d, b = %d, p es %p\n", a, b, p);
  
  *p = 77;
  printf("a = %d, b = %d, p es %p\n", a, b, p);

  return 0;
}
```

Explicar el comportamiento del programa. 

### Ejercicio 3: Funciones por referencia

#### Uso de punteros como parámetros y argumentos

- Escribe una función que:

  * Tome un parámetro de tipo puntero a entero sin signo.
  * Modifique el valor de la variable apuntada por el puntero dado
    de forma que el nuevo valor sea 1 más, pero si llega a 5, vuelva
    a 0. Es decir, la variable apuntado variará su valor cuando se 
    llame repetidamente a la función en la forma: 0, 1, 2, 3, 4, 0, ...

- Comprueba la función en el programa principal llamando 15 veces 
  a la misma. 

#### Paso de un puntero a otra función

- Escribe una función que:
  
  * Tome como parámetros dos punteros a double. 
  * Asumiendo que las variables a las que apuntan los parámetros
    representan un número complejo mediante módulo, argumento (ángulo),
  * Modifique sus valores para que representen el mismo número complejo
    mediante parte real y parte imaginaria. 

- Escribe el programa principal para comprobar la función anterior.

  * De verdad... Comprobadlo. Lo más normal es que no os salga bien
    a la primera. Hay una cosa en la que es habitual equivocarse. 

- Escribe una función adicional, en este caso:
  
  * La función toma dos parámetros de tipo puntero a double. 
  * Donde las variables apuntadas por dichos punteros representan
    un número complejo (igual que antes, en forma modulo, argumento).
  * Se debe obtener la representación en la forma parte real e imaginaria.
  * Se debe modificar la parte imaginaria para que el resultado sea
    el conjugado del número original (conjugado significa que se cambia
    de signo la parte imaginaria).
  * Es **obligatorio** usar la función anterior. 


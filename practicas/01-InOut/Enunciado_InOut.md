
# Programación para Sistemas

## Ejercicios Entrada / Salida y Tipos de datos

---

## Ejercicio 1: Multiplicar 2 números enteros

Escribir un programa que:

1. Lea dos números enteros desde el *canal de Entrada estándar* (el teclado)
1. Los divida y guarde el resultado en una variable
1. Escriba el resultado en el canal de salida

---

### Ejercicio 1: Método para resolver

1. Hay que escribir el programa vacío (siempre):

```c
    int main() {
        return 0;
    }
```

1. ¡Compilar!

```bash
    gcc -ansi -pedantic -Wall -Wextra -o mi_programa.out mi_programa.c
```

> Nota: la extensión de programa (.out) no es necesaria y en Linux no se suele
usar, pero la indico para se distinga mejor el programa como tal (y para poder
ignorarlos en git)

### Ejercicio 1: Siguientes pasos

1. En la mayoría de las ocasiones hay que añadir
archivos de cabeceras, en este caso: `#include <stdio.h>`
1. Declarar las variables necesarias (escogiendo *bien* el tipo)
1. Escribir las sentencias (consultar `scanf` y `printf`). Consulta los ejemplos
en las carpetas de años anteriores (2021, 2022 ó 2023).
1. Comprobar el programa:

```bash
    ./mi_programa.out
```

---

### Ejercicio 1: Comentarios

* Observe que para `scanf` hace falta poner un caracter
`&` delante de la variable. Ese es el símbolo del
operador *address of* que se explicará más adelante.
* Es importante compilar cada vez que se escribe algo
nuevo para evitar introducir *demasiados* errores en
el código fuente.
* Si no hay errores o avisos el compilador no muestra
nada (simplemente la terminal vuelve a mostrar el
*prompt*)

---

## Ejercicio 2: El tipo char

Escribir un programa para que:

1. Lea del canal de entrada una letra (char)
1. Escriba su valor.
1. Modifique su valor sumando 3 (pruebe con otros enteros,
especialmente, pruebe a sumar 256).
1. Escriba el nuevo valor.
1. Escriba su valor, pero utilizando %d como especificador de
conversión.

---

### Ejercicio 2: Comentarios

Una vez resuelto es muy conveniente probar el programa
y tratar de explicar el comportamiento del mismo.

Algunos conceptos importantes respecto de este ejemplo
son:

* `char` es un tipo *numérico* y *alfanumérico* según el
contexto.
* Los tipos numéricos pequeños se pueden desbordar con
resultados impredecibles (o no)

---

## Ejercicio 3: Calculos en coma flotante

En la librería estándar de C se pueden encontrar la
mayor parte de las funciones típicas del cálculo
matemático. Por ejemplo, funciones trigonométrica,
logaritmicas, raíces cuadradas, etc.

Escribir un programa que:

1. Lea un número en coma flotante del canal de entrada estándar
1. Calcule el logaritmo del número
1. Escriba el resultado en el canal de salida

---

## Ejemplo 3: Compilación

Son los anteriores, pero a tener en cuenta:

* Hace falta incluir la cabecera de las funciones matemáticas (`math.h`).
* Es *muy probable* que haya falta enlazarlas
explícitamente, es decir, el comando para compilar
queda como:

```bash
    gcc -ansi -pedantic -Wall -o mi_programa.exe mi_programa.c -lm
```

* Al probar el programa comprobar qué pasa cuando se
introduce un número negativo.

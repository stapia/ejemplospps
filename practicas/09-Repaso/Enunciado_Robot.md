
# Programación para Sistemas

**Utiliza** los ejemplos en la carpeta correspondiente como base para usar tanto 
**GNU PLOT** como **Glut**.

## Ejercicio de repaso 1: Mueve un robot

Se desea mostrar la trayectoria que sigue un robot que se mueve en un plano 
de coordenadas (x, y). 

Para ello se dispone de un archivo de texto que contiene las ordenes que ha ejecutado el robot:

```
# Este archivo muestra las ordenes del robot.
# Cada orden consta de una letra [A, G] que indica si avanza o gira,
# Para la orden de avance, A, se indica cuanto avanza (por defecto 1),
# Para la ordena de giro, G, se indica el ángulo en grados, 
# el signo indica la orientación del giro.
A 2
A 3
# Esto es un comentario, se debe ignorar
G -90
A
G +99
A
A 4
G +80
A 2
G -120
A 2
G +90
A 5
G +90
A 7
G +45
A 5
```

* Escribe un programa que:

  1. Lea el archivo de ordenes dado por el primer argumento (de `main`).
  2. Calcule los puntos x, y de la trayectoria por la que se mueve (se supone que gira en sitio).
  3. Muestre el resultado usando gnuplot (si es necesario ajusta la escala del gráfico).

* Se **recomienda** escribir el programa en base a funciones, por ejemplo:

  1. Un función para leer las órdenes y obtener un array con ellas.
  2. Una función para calcular los puntos a partir de las órdenes. 
  3. Una función para escribir en el *pipeline* de gnuplot los puntos obtenidos.

## Ejercicio de repaso 2: Mueve un robot con orden de *Repetir*

Modifica el programa anterior de manera que: 

* Introduzca en el archivo de órdenes una nueva *orden*, R (repetir), que repita
  las últimas N órdenes, M veces. Por ejemplo:

```
A 2
A 3
G -90
R 2 3
# Se repiten 3 veces las 2 órdenes A3 y G -90
```

El ejemplo anterior debería dibujar un cuadrado. 

## Ejercicio de repaso 3: Mueve un robot mostrando una animación

En este ejercicio se va a introducir que el robot pueda tener una velocidad. 

En concreto, se supone que la velocidad inicial del robot es 1 y se puede aumentar
con una nueva orden: V (Velocidad), que fija la velocidad del robot a un nuevo 
valor. Por ejemplo: 

```
# Velocidad inicial 1
A 2
# Incrementa la velocidad a 2:
V 2
A 3
# Para la rotación simplemente asume que tarda tiempo constante.
G -90
A
G +99
A
# Más rápido:
V 5
A 4
G +80
# Más lento
V 1
A 2
```

* Escribe el programa que permita:

  1. Leer las órdenes
  2. Mostrar en una animación con Glut el movimiento del robot.


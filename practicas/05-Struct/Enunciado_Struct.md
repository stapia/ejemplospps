
# Programación para Sistemas

## Ejercicios Estructuras (struct)

### Ejercicio 1

* Escribe un programa que:

  1. Defina un tipo estructuctura que permita guardar: una etiqueta de hasta 31 carácteres y un número en coma flotante.
  2. Defina una función que escriba en el canal de salida los datos de un struct suponiendo que el número es una cantidad física y la etiqueta las unidades de medida. 
  3. Defina una función que tome un struct como parámetro que contiene una unidad con un prefijo de multiplo: k, M ó G y retorne un struct con el valor correcto de la cantidad física pasada a la unidad sin el prefijo. Por ejemplo, si el argumento es `2.4 kg` la función debe retornar `2400 g`.
  4. En la función `main`: declare una variable del tipo struct definido, asigne a la estructura un valor de ejemplo, llame a la función para escribir, haga el cambio de unidades y muestre el resultado.



# Programación para Sistemas

## Ejercicios Memoria Dinámica en 2 dimensiones

### Ejercicio 1

* Escribe un programa que:

  1. Lea los datos de una matriz cuadrada de números en coma flotante desde un archivo 
  2. Los guarde en una matriz dinámica formada mediante un vector a punteros que
     apuntan a cada una de las filas.
  3. Escriba en el canal de salida los elementos de la diagonal principal en 
     orden inverso. 

Ejemplo de contenido del archivo:

```
3
1.5 5.6 9.99
8.2 2.1 7.21
5.5 3.6 8.87
```

Ejemplo de salida:

```
8.87
2.1
1.5
```

### Ejercicio 2

* Escribe un programa que:

  1. Lea puntos en dos dimensiones de la entrada estándar y los guarde
     en una matriz dinámica formada por un número arbitrario de filas 
     de tamaño fijo igual a tres números en coma flotante.
  2. Suponiendo que el primer valor de cada fila es la masa de un punto
     y las otras dos las coordenadas x e y de ese punto, calcule el momento de inercia respecto del eje Z tomando como origen el centroide de los puntos dados.

Para calcular el centroide de masas de un conjunto de puntos:

El centroide de un conjunto de puntos con coordenadas (x_i, y_i) en un plano cartesiano y masas asociadas m_i se calcula mediante las siguientes fórmulas:

**Coordenada x del centroide:**

    x_c = (Σ (m_i * x_i)) / (Σ m_i)

**Coordenada y del centroide:**

    y_c = (Σ (m_i * y_i)) / (Σ m_i)

Donde:
- x_i, y_i: Coordenadas del punto i.
- m_i: Masa asociada al punto i.
- n: Número total de puntos.

El centroide (x_c, y_c) es el punto en el cual el sistema de puntos estaría en equilibrio si estuviera sujeto a la gravedad.

Y para calcular el momento de inercia del eje Z pasando por el centroide:

El momento de inercia respecto al eje \( Z \), pasando por el centroide del sistema, se calcula utilizando las siguientes fórmulas:

**Momento de inercia:**

    I_z = Σ m_i * [(x_i - x_c)^2 + (y_i - y_c)^2]

Donde:
- \( m_i \): Masa del punto \( i \).
- \( x_i, y_i \): Coordenadas del punto \( i \).
- \( x_c, y_c \): Coordenadas del centroide del sistema,

El momento de inercia \( I_z \) mide la resistencia del sistema de puntos a rotar alrededor del eje \( Z \), considerando que el eje pasa por el centroide.


### Ejercicio 3

Escribe un programa que:

* Lea líneas de texto de tamaño arbitrario desde un archivo.
* Las guarde consecutivas en memoria.
* Guarde un vector de punteros donde cada puntero apunte al 
  contenido de una línea. 
* Para comprobar escriba las líneas en orden inverso.
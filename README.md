# README #

En este repositorio voy a publicar las transparencias y ejemplos de clase de PPS

### Estructura del repositorio

* Los ejemplos del curso se indicarán con el año del cuatrimestre que toque. Por ejemplo: el curso 2021-2022 estará en la carpeta "2021".
* La documentación común a todos los curso estará en "comun"

### A tener en cuenta

* Los ejemplos estarán escritos en ANSI C (C-90).
* Por lo tanto se deberían poder compilar en cualquier compilador que implemente ese estándar (por ejemplo gcc).
* Los ejemplos en sí mismos no "explican" nada ni son la única manera de resolver un problema ni tampoco necesariamente la mejor.

### Cómo modificar los ejemplos

Típicamente, como parte del trabajo en la asignatura, es posible que queráis modificar los ejemplos. Para hacer esto lo más práctico es hacer una rama que trabajéis en esa rama, de esa manera podréis tener una versión propia de los ejemplos y, a la vez, podréis ser capaces de recuperar la versión de clase con facilidad. Los comandos en la terminal para hacer esto son:

***

```bash
git branch mi-rama-1
git checkout mi-rama-1
```

***
  
Donde `mi-rama-1` es el nombre de la nueva rama. El primer comando crea la rama y el segundo hace que sea la rama activa. 
Naturalmente podéis crear tantas ramas como necesitéis simplemente cambiando el nombre. Para guardar los cambios hay que hacer:

***

```bash
git commit -am"texto de la versión"
```

***

Y para volver a la versión original:

***

```bash
git checkout master
```

***

**¡Cuidado!** los cambios hechos de esta manera serán locales en vuestra máquina.

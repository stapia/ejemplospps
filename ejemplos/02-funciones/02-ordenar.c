
#include<stdio.h>

void ordenar(int *x, int *y)
{
    if ( *x < *y) {
        int aux = *x;
        *x = *y;
        *y = aux;
    }
}

void ordenar3(int *x, int *y, int *z) {
    ordenar(x,y);
    ordenar(y,z);
    ordenar(x,y);
}

int main() {
    int a, b, c;
    scanf("%d %d", &a, &b);
    {
        int x = a, y = b;
        if (x < y) {
            int aux = x;
            x = y;
            y = aux;
        }
        printf("x,y : %d %d\n", x, y);
    }
    {
        int *x = &a, *y = &b;
        if ( *x < *y) {
            int aux = *x;
            *x = *y;
            *y = aux;
        }
    }
    printf("%d %d\n", a, b);

    scanf("%d %d %d", &a, &b, &c);
    ordenar3(&c, &b, &a);
    printf("%d %d %d\n", a, b, c);
    return 0;
}
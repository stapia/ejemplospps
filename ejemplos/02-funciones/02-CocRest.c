
#include <stdio.h>

int cociente_resto(int dividendo, int divisor, int *p_resto) {
  int cociente;
  *p_resto = dividendo % divisor;
  cociente = dividendo / divisor;
  printf("Funcion dice: El cociente es %d y el resto es %d\n", cociente, *p_resto);
  return cociente;
}

int main() {
  int a = 20;
  int c = 0, r = -8;

  { /* Bloque que emula la función */
    /* Paso de parámetros */
    int dividendo = a, divisor = 3;
    int *p_resto = &r;
    /* Resto de la rutina */
    int cociente;
    *p_resto = dividendo % divisor;
    cociente = dividendo / divisor;
    printf("Funcion dice: El cociente es %d y el resto es %d\n", cociente, *p_resto);
    /* Asigno el retorno */
    c = cociente;
  }
  printf("El cociente es %d y el resto es %d\n", c, r);

  /* Todo el bloque anterior se resume en: */
  c = cociente_resto(a, 3, &r);
  printf("El cociente es %d y el resto es %d\n", c, r);

  return 0;
}

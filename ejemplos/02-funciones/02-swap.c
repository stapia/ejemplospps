
#include<stdio.h>

void swap_mal(int a, int b) {
    int aux;
    aux = a;
    a = b;
    b = aux;
    printf("swap mal dice: a = %d, b = %d\n", a, b);
}

void swap(int *pa, int *pb) {
    int aux;
    aux = *pa;
    *pa = *pb;
    *pb = aux;
}

int main() {
    int a = 20, b = 13;
    swap_mal(a, b);
    printf("main dice: a = %d, b = %d\n", a, b);
    swap(&a, &b);
    printf("main dice: a = %d, b = %d\n", a, b);
    return 0;
}
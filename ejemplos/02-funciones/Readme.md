
# Funciones en C

Los ejemplos en esta carpeta están clasificados por los conceptos
que ilustran, la clasificación viene dada por el número que prefija
el nombre de los archivos.

## Ejemplos con parámetros por valor y retorno (número 01-)

Son dos ejemplos que toman valores y producen un resultado. 

En el ejemplo 01-maximo.c se puede encontrar un par de fragmentos 
de código para comprender mejor el paso de parámetros a las funciones.

## Ejemplos con parámetros por referencia (número 02-)

Antes de pasar a los ejemplos, es muy conveniente seguir las 
instrucciones del documento `02-Punteros.md`. Ese documento 
contiene dos ejemplos a ejecutar en (pythontutor.com)[pythontutor.com] que ilustran el concepto de puntero.

El resto de ejemplo usan punteros para declarar función con 
parámetros por referencia. 

Se recomienda consultar especialmente `02-CocRest.c` porque
incluye un fragmento de código fuente para ilustrar el comportamiento de un paso por
referencia mediante un parámetro de tipo puntero. 

En `02-swap.c` se puede encontrar un ejemplo que ilustra que va mal cuando 
es necesario el paso de parámetros por referencia y no se usa. 

En `02-ordenar.c` se puede encontrar un ejemplo donde hay que usar punteros para 
pasarla a una función que se van a usar. Este es un ejemplo un poco más complicado,
se recomienda *estudiarlo* con cuidado hasta que se entienda completamente. 

## Ejemplos con funciones que usan canales de entrada - salida (número 3-)

Los ejemplos con el prefijo `3-` ilustran funciones *peculiares* en
su diseño porque, en vez de usar parámetros o el retorno, usan los
canales de entrada o salida para obtener o devolver información.

Lo importante es observar que en estos casos se elude la declaración
de los correspondiente parámetros (o retorno) y se sustituye por la
declaraciones de variables locales. 
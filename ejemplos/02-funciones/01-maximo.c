
#include <stdio.h>

int maximo(int x, int y) {
  int max;
  if (x > y) {
    max = x;
  } else {
    max = y;
  }
  return max;
}

int main() {
  int a, b, c, d, max;
  scanf("%d %d", &a, &b);
  { /* Bloque dónde se calcula el máximo de a y b */
    int x = a, y = b;
    if (x > y) {
      max = x;
    } else {
      max = y;
    }
  }
  printf("El max es %d\n", max);
  scanf("%d %d", &c, &d);
  { /* Bloque dónde se calcula el máximo de c y d,
    ATENCIÓN sólo cambia la primera línea respecto del anterior */
    int x = c, y = d;
    if (x > y) {
      max = x;
    } else {
      max = y;
    }
  }
  /* Ahora con funciones ... */
  printf("El max es %d\n", max);
  max = maximo(a, b);
  printf("El max es %d\n", max);
  max = maximo(a, b + 5);
  printf("El max es %d\n", max);
  return 0;
}

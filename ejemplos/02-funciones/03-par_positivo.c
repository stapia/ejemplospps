
#include<stdio.h>

void par_positivo(int numero) {
    printf("%d es %s\n", numero, numero % 2 ? "impar" : "par");
    printf("%d es %s\n", numero, numero > 0 ? "positivo" : "cero o negativo");
}

int main()
{
    par_positivo(2);
    par_positivo(-2);
    par_positivo(3);
    par_positivo(-3);
    par_positivo(0);
    return 0;
}

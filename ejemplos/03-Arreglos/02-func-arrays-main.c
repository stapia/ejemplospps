
#include <stdio.h>

#include "02-func-arrays.h"

#define DIM 5

int main() {
  double arr[DIM];
  double media;
  printf("Declaro un array de size: %ld\n", sizeof(arr));
  leer(arr);
  media = calcular_media(arr, DIM);
  printf("La media es %8.2f\n", media);
  valor_absoluto(arr + 2, 3);
  escribir(arr, DIM);
  return 0;
}

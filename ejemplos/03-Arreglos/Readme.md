
# Utilización de Arrays (arreglos)

## Ejemplo 1: Uso básico de arrays

### Explicación

El programa: 

1. Lee una serie de datos en coma flotante de la entrada estándar y
lo guarda en un array,
1. Calcula la media de esos datos,
1. La escribe en la salida estándar,
1. Modifiqua los datos del array de manera que se sustituyan los 
valores negativos por su valor absoluto.
1. Muestra los datos modificados.

### A tener en cuenta

* Es habitual utilizar una *macro* para definir el tamaño de un
  array, está definida en la línea 4.
* En la línea 13 se muestra cómo se puede obtener el puntero al
  *object* donde se va a guardar el valor leído. 
* Cuidado, para leer double se usa `%lf` en el scanf.
* En la línea 21 se puede ver cómo se muestran datos con un formato
  de *columna* de ancho fijo. Y con un número de decimales dado. 
* Para no tener que escribir todas las veces la entrada se puede
  compilar y ejecutar el programa haciendo:

```bash
gcc -ansi -pedantic -Wall -Wextra -o 01-Arrays.out 01-Arrays.c
./01-Arrays.out < 01-datos.txt
```

## Ejemplo 2: Uso de arrays con funciones

### Explicación

Este ejemplo ilustra:

* La división del código fuente en varios archivos, y
* El uso de arrays como parámetros de funciones.

Para entender cómo hay que compilar varios archivos en C hay que 
consultar el breviario (C en Resumen) en:

[Breviario organización del código](https://c-en-resumen.readthedocs.io/es/latest/breviario.html#organizacion-del-codigo-en-c)

Al final de la sección se encuentran los comandos para compilar, aunque básicamente
para compilar hay que añadir la opción `-c` al comando `gcc`.

Este ejemplo consiste en poner en funciones el código fuente desarrollado en el ejemplo
anterior. 

### A tener en cuenta

Debe observarse que:

* Los arrays se pasan como parámetros usando la dirección de memoria al
  primer elemento y el tamaño (el número de elementos).
* Cuando el arrays no se va a modificar se marca como `const`, eso 
  significa que el array es una entrada a la función (aunque se 
  pase como referencia)
* En la implementación de la función se puede usar el operador `[ ]` 
  aplicado al puntero **exactamente** igual que lo hariamos con un array. 
* En la línea 11 de *02-func-arrays-main.c* se muestra el *tamaño*
  del array. ¿Cuanto debería salir?
* En la línea 11 de *02-func-arrays.c* se muestra el *tamaño* del array. 
  ¿Cuanto debería salir?
* La declaración de `leer` es la misma en el .h y en el .c, aunque
  no está escrita de la misma manera.
* ¿Qué significa y qué consecuencia tiene poner `arr + 2` en la línea
  15 de *02-func-arrays-main.c*.
* Para ejecutar el programa se pueden usar los mismos datos que 
  en el ejemplo anterior.
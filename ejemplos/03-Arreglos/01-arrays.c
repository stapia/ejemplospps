
#include <stdio.h>

#define DIM 5

int main() {
  double arr[DIM];
  double media;
  int i;

  for (i = 0; i < DIM; ++i) {
    scanf("%lf", arr + i);
    /* Toda la línea anterior casi equivale a:
    scanf("%lf", &x);
    array[i] = x;
    printf("x = %f\n", x);
    Pero esta alternativa necesita de la variable auxiliar x.
    */
    /* Y sí es totalmente equivalente a:
    scanf("%lf", &(arr[i]) );
    */
  }

  media = 0;
  for (i = 0; i < DIM; ++i) {
    media += arr[i];
  }
  media /= DIM;
  printf("La media es %8.2f\n", media);

  for (i = 0; i < DIM; ++i) {
    if (arr[i] < 0) {
      arr[i] = -arr[i];
    }
  }

  printf("( %.2f", arr[0]);
  for (i = 1; i < DIM; ++i) {
    printf(", %.2f", arr[i]);
  }
  printf(" )\n");

  return 0;
}

#include <GL/glut.h>
#include <math.h>
#include <stdio.h>

// Variables globales para controlar la posición del punto
float x_position = -10.0f;  // Posición inicial en el eje X
float angle      = 0.0f;    // Angulo
float y_position = 0.0f;    // eje Y
float step       = 0.1f;    // Incremento en cada frame (velocidad)

// Función para actualizar la posición del punto
void update(int /* value */) {
  x_position += step;  // Mueve el punto hacia la derecha

  if (x_position > 10.0f) {  // Si llega al borde derecho, reinicia la posición
    x_position = -10.0f;
  }

  // En vertical con un seno
  y_position = 5.0 * sin(angle);
  angle += 0.05;

  glutPostRedisplay();           // Marca la ventana para ser redibujada
  glutTimerFunc(16, update, 0);  // Llama a update de nuevo en ~16 ms (~60 FPS)
}

// Función para dibujar en la ventana
void display() {
  glClear(GL_COLOR_BUFFER_BIT);  // Limpia la pantalla

  // Dibuja el punto
  glPointSize(8.0f);  // Tamaño del punto
  glBegin(GL_POINTS);
  glVertex2f(x_position, y_position);  // Coordenadas del punto
  glEnd();

  glutSwapBuffers();  // Intercambia los buffers para mostrar la imagen
}

// Configuración inicial de la ventana
void init() {
  glClearColor(0.0, 0.0, 0.0, 1.0);  // Fondo negro
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(-10.0, 10.0, -10.0, 10.0);  // Sistema de coordenadas 2D
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutInitWindowSize(800, 600);               // Tamaño de la ventana
  glutCreateWindow("Animacion 2D con GLUT");  // Título de la ventana

  init();                                 // Configuración inicial
  glutDisplayFunc(display);               // Función de dibujo
  glutTimerFunc(0, update, 0 /*value*/);  // Inicia la animación

  glutMainLoop();  // Bucle principal de GLUT
  return 0;
}

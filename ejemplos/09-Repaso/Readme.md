
# Repaso: Ejemplos con salida gráfica

## Gráficos con gnu plot, `09-1-dibujar_puntos.c`

### Compilar el ejemplo

**ATENCIÓN** 

Este ejemplo **no** se puede compilar con ANSI C. Se debe compilar con:

```bash
gcc -Wall -Wextra 09-1-dibujar_puntos.c -o 09-1-dibujar_puntos.out
```

Porque popen y pclose no forman parte de ANSI C.

### Ejecutar el ejemplo:

Para que el ejemplo se pueda ejecutar hay que instalar GNU Plot:

```bash
sudo apt-get install gnuplot
```

Y luego ejecutar con:

```bash
./09-1-dibujar_puntos.out 09-1-puntos.txt
```

### Explicación:

La función popen permite *abrir* el archivo del canal del entrada
de un comando que se pasa como una cadena alfanumérica a la terminal.

## Gráficos con Glut, `09-2-usar-glut.c`

### Compilar el ejemplo:

**ATENCIÓN** 

Para compilar este ejemplo hay que tener instalada la librería glut3 para *desarrollo*:

```bash
sudo apt-get install freeglut3-dev
```

Y hay que incluir que se enlacen sus librerías al compilar (y también la librería 
de math), el comando para compilar queda: 

```bash
gcc -Wall -Wextra 09-2-usar-glut.c -o 09-2-usar-glut.out -lGL -lGLU -lglut -lm
```

Se ejecuta normalmente. 

### Explicación

Este es un ejemplo donde se usan *callbacks*, es decir, funciones que el programador
escribe, pero que se llaman dentro de un contexto que no es su propio código fuente.

En este caso, las siguiente funciones son *callbacks*:

* `display`: Esta función se llama *desde* glut para ejecutar el código que dibuja.
* `update`: Esta función se llama *desde* glut cuando pasa cierto tiempo para 
  actualizar el *estado* del dibujo. 

Por último, `glutMainLoop` se dice que es una función *irás y no volverás* porque
en ese momento el programador le pasa el control a un contexto que no es el suyo
(en este caso la librería glut) y su código se ejecuta por otros medios (en
este caso a través de las *callbacks*).
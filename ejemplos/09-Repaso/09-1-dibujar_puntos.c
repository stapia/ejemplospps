#include <stdio.h>
#include <stdlib.h>

#define INITIAL_CAPACITY 128 /* Capacidad inicial para el array dinámico */

/* Estructura para almacenar un punto 2D */
typedef struct {
  double x;
  double y;
} Point;

/* Función para leer puntos desde un archivo y almacenarlos en un array dinámico */
Point *read_points(const char *filename, int *count) {
  int capacity = INITIAL_CAPACITY;
  double x, y;
  Point *points;
  FILE *file = fopen(filename, "r");
  if (file == NULL) {
    fprintf(stderr, "Error al abrir el archivo: %s\n", filename);
    exit(1);
  }

  points = calloc(capacity, sizeof(Point));
  if (points == NULL) {
    fprintf(stderr, "Error al asignar memoria.\n");
    exit(2);
  }

  *count = 0;

  while (fscanf(file, "%lf %lf", &x, &y) == 2) {
    if (*count >= capacity) {
      capacity *= 2;
      points = realloc(points, capacity * sizeof(Point));
      if (points == NULL) {
        fprintf(stderr, "Error al volver a reservar memoria.\n");
        exit(3);
      }
    }

    points[*count].x = x;
    points[*count].y = y;
    (*count)++;
  }

  fclose(file);
  return points;
}

/* Función para enviar los puntos a gnuplot directamente */
void plot_points_with_gnuplot(Point *points, int count) {
  int i;
  FILE *gnuplotPipe = popen("gnuplot -persist", "w");
  if (gnuplotPipe == NULL) {
    fprintf(stderr, "Error al abrir gnuplot.\n");
    exit(EXIT_FAILURE);
  }

  /* Configuración de gnuplot */
  fprintf(gnuplotPipe, "set title 'Puntos 2D'\n");
  fprintf(gnuplotPipe, "set xlabel 'X'\n");
  fprintf(gnuplotPipe, "set ylabel 'Y'\n");
  fprintf(gnuplotPipe, "set grid\n");
  fprintf(gnuplotPipe,
          "plot '-' with linespoints pointtype 7 pointsize 1 title 'Líneas y Puntos'\n");

  /* Enviar los puntos */
  for (i = 0; i < count; i++) {
    fprintf(gnuplotPipe, "%f %f\n", points[i].x, points[i].y);
  }

  fprintf(gnuplotPipe, "e\n"); /* Finaliza los datos para gnuplot */
  fflush(gnuplotPipe);         /* Vacía el buffer de escritura */
  pclose(gnuplotPipe);
}

int main(int argc, char *argv[]) {
  const char *filename;
  int count;
  Point *points;

  if (argc != 2) {
    printf("Uso: %s <archivo de puntos>\n", argv[0]);
    return 4;
  }

  filename = argv[1];

  /* Leer los puntos desde el archivo */
  points = read_points(filename, &count);

  printf("Se han leído %d puntos.\n", count);

  /* Enviar los puntos a gnuplot */
  plot_points_with_gnuplot(points, count);

  /* Liberar la memoria */
  free(points);

  return EXIT_SUCCESS;
}

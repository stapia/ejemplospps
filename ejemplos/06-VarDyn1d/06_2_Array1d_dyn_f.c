
#include <stdio.h>
#include <stdlib.h>

double calcular_media(const double *array, unsigned tam) {
  unsigned int i;
  double suma = 0;
  for (i = 0; i < tam; ++i) {
    suma += array[i];
  }
  return suma / tam;
}

double *leer_array_con_tam(unsigned tam) {
  double *array;
  unsigned i;
  /* equivale a array = new double[tam]; en Java */
  array = calloc(tam, sizeof(double));
  for (i = 0; i < tam; ++i) {
    scanf("%lf", array + i);
  }
  return array;
}

double *leer_array(unsigned *ptam) {
  double *array;
  unsigned i;
  scanf("%u", ptam);
  /* equivale a array = new double[tam]; en Java */
  array = calloc(*ptam, sizeof(double));
  for (i = 0; i < *ptam; ++i) {
    scanf("%lf", array + i);
  }
  return array;
}

int main(int argc, char *argv[]) {
  double *array;
  double media;
  unsigned tam;

  if (argc == 1) {
    scanf("%u", &tam);
    array = leer_array_con_tam(tam);
  } else if (argc >= 2) {
    /* argv[1] -> unsigned */
    sscanf(argv[1], "%u", &tam);
    array = leer_array_con_tam(tam);
  }

  media = calcular_media(array, tam);

  printf("La media es: %f\n", media);

  free(array);

  return 0;
}

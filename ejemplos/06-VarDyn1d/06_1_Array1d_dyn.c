
#include <stdio.h>

int main(int argc, char const *argv[]) {
  double *array; /* No es un array porque no es double array[5]; */
  unsigned i, tam;

  /* Para ignorar los parámetros de main */
  (void)argc;
  (void)argv;

  scanf("%u", &tam);
  /* equivale a array = new double[tam]; en Java */
  array = calloc(tam, sizeof(double)); /* A partir de ahora array es un array dinámico */
  for (i = 0; i < tam; ++i) {
    scanf("%lf", array + i); /* array + i es una dirección */
  }

  printf("( ");
  for (i = 0; i < tam; ++i) {
    printf("%lf ", array[i]); /* array[i] es un double */
  }
  printf(")\n");

  free(array); /* En C debemos liberar la memoria usada!! */

  return 0;
}


# Arrays 1d dinámicos.

## Ejemplo 06_1_Array1d_dyn

### Explicación:

El programa: 

* Lee del canal de entrada un número entero, `tam`, crea un array dinámico de ese tamaño y lee `tam` datos en coma flotante y los guarda en el array.
* Escribe los datos leídos en el canal de salida.

### A tener en cuenta:

* `calloc` devuelve `void*`, pero se convierte a `double*`.
* `calloc` inicializa a cero toda la memoria reservada.
* `array + i` es equilante a `&(array[i])`: la **dirección de memoria** del elemento `i` de tipo double. 
* `array[i]` es equivalente a `*(array + i)`: el **double** que ocupa la posición de índice `i` en el array.

## Ejemplo 06_2_Array1d_dyn_f

### Explicación:

Más o menos lo mismo de antes, pero con funciones.

El programa:

* Define una función para calcular la media y otras dos para leer datos de un tamaño dado o de un tamaño que se lee del canal de entrada. 
* La función `main` utiliza sus parámetros: si se pasa un número ese es el número de elementos del array. 
* Calcula la media.
* Libera la memoria.

## A tener en cuenta:

* La manera más fácil de hacer conversiones desde cadena a otros tipos es usar `sscanf`. Es lo mismo que `scanf`, pero el origen de los datos es una cadena que se pasa como primer parámetro. Esto es verdaderamente **útil**. 
* Ambas funciones de lectura reservan memoria dinámica y devuelven la dirección de la variable reservada. Se dice que: "**transfieren la propiedad al código cliente**" o alguna otra frase similar, lo que significa es que la función **reserva**, pero es responsabilidad del que llama **liberar** la memoria reservada. 
* Ambas funciones retornan la dirección de memoria al primer elemento del array (un puntero a double) y para ambas tiene que estar previsto cómo se gestiona el tamaño del array.
* Un puntero es un tipo **simple**, en C no se puede retornan arrays estáticos, pero **sí** arrays dinámicos porque se puede retornar un puntero.
* **No** hay ninguna diferencia entre una función media que acepte un array dinámico o uno estático.
* Explica las diferentes formas en que está declarado `tam` en las funciones. 
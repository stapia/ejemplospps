#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INITIAL_CAPACITY 8 /* Capacidad inicial de filas */
#define ROW_SIZE 32        /* Tamaño fijo de cada fila */

typedef struct {
  char (*data)[ROW_SIZE]; /* Matriz dinámica de filas fijas */
  int size;               /* Número de filas actualmente ocupadas */
  int capacity;           /* Capacidad máxima actual */
} string_array_t;

/* Función para inicializar la matriz */
string_array_t *create_string_array_t() {
  string_array_t *sa = (string_array_t *)malloc(sizeof(string_array_t));
  if (!sa) {
    fprintf(stderr, "Error: no se pudo asignar memoria para la struct\n");
    return NULL;
  }

  sa->data = calloc(INITIAL_CAPACITY, ROW_SIZE);
  if (!sa->data) {
    fprintf(stderr, "Error: no se pudo asignar memoria\n");
    free(sa);
    return NULL;
  }

  sa->size     = 0;
  sa->capacity = INITIAL_CAPACITY;
  return sa;
}

/* Función para aumentar la capacidad de la matriz */
int resize_string_array_t(string_array_t *sa) {
  int new_capacity          = sa->capacity * 2;
  char(*new_data)[ROW_SIZE] = realloc(sa->data, new_capacity * ROW_SIZE);
  if (!new_data) {
    fprintf(stderr, "Error: no se pudo reservar más memoria\n");
    return 0;
  }

  sa->data     = new_data;
  sa->capacity = new_capacity;
  return 1;
}

/* Función para agregar una cadena a la matriz
 * PRECOND: strlen(str) < ROW_SIZE */
int add_str(string_array_t *sa, const char *str) {
  if (sa->size == sa->capacity) {
    if (!resize_string_array_t(sa)) {
      return 0;
    }
  }

  strcpy(sa->data[sa->size], str);
  sa->size++;
  return 1;
}

/* Función para liberar la memoria de la matriz */
void free_string_array_t(string_array_t *sa) {
  if (!sa) return;

  free(sa->data);
  free(sa);
}

/* Función para mostrar la matriz en orden inverso */
void print_string_array_t_reverse(string_array_t *sa) {
  int i;
  if (!sa) return;

  for (i = sa->size - 1; i >= 0; i--) {
    printf("%s\n", sa->data[i]);
  }
}

/* Función principal: probar con echo "hola mundo.adios;fin" | ./programa */
int main() {
  char buffer[ROW_SIZE]; /* Buffer temporal para cada palabra */
  string_array_t *sa = create_string_array_t();
  if (!sa) {
    return 1;
  }

  /* Leer la entrada estándar: lee hasta un separador y luego lo
   * QUITA mediante %*c (lee e ignora un caracter). No sigue leyendo
   * si encuentra dos separadores seguidos */
  while (scanf("%31[^,.;\t \n]%*c", buffer) == 1) {
    fprintf(stderr, "Leido: `%s'\n", buffer);
    if (!add_str(sa, buffer)) {
      fprintf(stderr, "Error: no se pudo agregar la palabra\n");
      free_string_array_t(sa);
      return 1;
    }
  }

  /* Mostrar las filas en orden inverso */
  print_string_array_t_reverse(sa);

  /* Liberar la memoria asignada */
  free_string_array_t(sa);

  return 0;
}

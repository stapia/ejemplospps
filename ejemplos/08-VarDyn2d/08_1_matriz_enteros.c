#include <stdio.h>
#include <stdlib.h>

/* */
typedef struct {
  int **data; /* Puntero a la matriz dinámica */
  int size;   /* Número de filas y columnas */
} matriz_t;

/* Función para crear la matriz y leer los datos desde un archivo */
matriz_t *read_matriz_t_from_file(FILE *file) {
  int size, i, j;
  matriz_t *m = malloc(sizeof(matriz_t));

  fscanf(file, "%d", &size);
  m->size = size;

  /* Reserva memoria para el vector de punteros */
  m->data = calloc(size, sizeof(int *));
  /* Reserva un vector dinámico por fila y guarda la dirección
   *  en el vector de punteros anterior */
  for (i = 0; i < size; i++) {
    m->data[i] = calloc(i+1, sizeof(int));
  }

  /* Lee los elementos de la diagonal por debajo de la diagonal */
  for (i = 0; i < size; i++) {
    for (j = 0; j <= i; j++) {
      fscanf(file, "%d", &(m->data[i][j]));
    }
  }

  return m;
}

/* Función para obtener el valor en un índice específico (i, j) */
int get_value(const matriz_t *m, int i, int j) {
  if (m == NULL || i < 0 || i >= m->size || j < 0 || j >= m->size) {
    fprintf(stderr, "Error: índices fuera de rango\n");
    return 0;
  }
  return j <= i ? m->data[i][j] : m->data[j][i];
}

/* Función para escribir la matriz completa en un archivo */
void write_matriz_t_to_file(const matriz_t *m, FILE *file) {
  int i, j;
  for (i = 0; i < m->size; i++) {
    for (j = 0; j < m->size; j++) {
      fprintf(file, "%d ", get_value(m, i, j));
    }
    fprintf(file, "\n");
  }
}

/* Función para liberar la memoria asignada */
void free_matriz_t(matriz_t *m) {
  int i;
  if (m == NULL) return;

  /* Primero hay que liberar la memoria de las filas */
  for (i = 0; i < m->size; i++) {
    free(m->data[i]);
  }
  free(m->data);
  free(m);
}

int main() {
  int i, j;
  matriz_t *m      = NULL;
  FILE *input_file = fopen("diagonal.txt", "r");
  if (input_file == NULL) {
    fprintf(stderr, "No se pudo abrir el archivo de entrada\n");
    return 1;
  }

  /* Leer la matriz desde un archivo */
  m = read_matriz_t_from_file(input_file);
  fclose(input_file);

  /* La escribe en la salida estándar */
  write_matriz_t_to_file(m, stdout);

  /* Obtener valores */
  i = 1, j = 2;
  printf("Valor en [%d][%d]: %d\n", i, j, get_value(m, i, j));
  i = 2, j = 1;
  printf("Valor en [%d][%d]: %d\n", i, j, get_value(m, i, j));

  /* Liberar la memoria asignada */
  free_matriz_t(m);

  return 0;
}

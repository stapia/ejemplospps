
# Usar variable dinámica en dos dimensiones

## Una matriz diagonal de enteros (08_1_matriz_enteros.c)

El programa:

* Lee una matriz diagonal desde un archivo donde se indica su tamaño
  y sus elementos por debajo de la diagonal principal por filas.
* Escribe la matriz completa en la salida estándard
* Y también un par de valores (para mostrar la función correspondiente)

### Explicación:

* Lo más importante: es una matriz formada a partir de un vector de punteros.
* Cada fila se reserva dinámica (atendiendo al número de elementos que 
  hay que guardar) y, por tanto, se necesita un puntero para apuntarla.
* Para guardar los punteros a las filas se usa un array dinámico cuyos 
  elementos son punteros. Como se genera dinámicamente necesitamos un 
  puntero a puntero (el int**).
* Para simplificar la gestión de la matriz se mete este int** y el 
  tamaño de la matriz (cuadrada) en un único tipo (struct). 

## Matriz dinámica con tamaño de fila fijo (08_2_array_string.c)

El programa:

* Lee de la entrada estándar una serie de cadenas alfanuméricas
  separadas por blancos o caracteres de puntuación.
* Las guarda en una variable dinámica cuyo tamaño se puede 
  aumentar arbitrariamente.
* Las escribe en orden inverso.

## Explicación

* Lo más importante: se usa un array dinámico que es un vector 
  cuyas componentes son cadenas alfanuméricas de tamaño fijo (32).
* Por eso se usa un *puntero a array*, es decir: `char (*data)[ROW_SIZE]`.
* El struct se usa para gestionar el tamaño máximo y el que realmente 
  se usa en una única variable. 
* La función `realloc` permite redimensionar el array.
* El especificador de conversión `%[]` permite leer *mientras* o *hasta* 
  (con ^) que encuentre los caracteres que se indican dentro de los corchetes. 


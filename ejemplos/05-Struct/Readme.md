
# Struct

## Definición y uso de struct

### Explicación:

El programa: 

* Define un tipo `struct`, `Datos`, con: una cadena, un número entero y un array de 3 números en coma flotante.
* Hace un alias del tipo (`typedef`) que sea `Datos_t`.
* Declara una variable de tipo `Datos_t`.
* Define una función que lee del canal estándar los datos de una variable `Datos_t`.
* Define una función que escribe en el canal de salida estándar los datos de una variable de tipo `Datos_t` en formato: `etiqueta : valor` con la cadena entre comillas dobles y el vector entre corchetes y con las componentes separadas por comas.

### A tener en cuenta:

* IMPORTANTE: hasta que se hace el typedef el tipo de la estructura es `struct datos` todo completo incluido `struct`.
* Como `Datos_t` tiene dentro arrays, estos arrays se *copian* cuando se pasan a cualquier función que tome como argumento un `Datos_t`. Esto no es muy eficiente y en algunas ocasiones se utilizan punteros para pasar estructuras porque es más eficiente. 
* Trata de calcular cuál es el tamaño de `Datos_t` y comprueba cuanto es según el programa. Busca qué significa *padding* y *memory alignment*.
* Comprueba cómo se usa `.` y `->`.
* Y puestos a mirar detalles... ¿El programa es seguro?
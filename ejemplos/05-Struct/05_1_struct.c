
#include <stdio.h>
#include <string.h>

struct Datos {
  char texto[100];
  int numero;
  double v[3];
  char nada; /* Quita este campo y comprueba qué pasa con sizeof*/
};

void leer(struct Datos *d) {
  scanf("%s %d", d->texto, &(d->numero));
  scanf("%lf %lf %lf", d->v, d->v + 1, &(d->v[2]));
}

typedef struct Datos Datos_t;

void escribir(Datos_t d) {
  printf("texto: \"%s\"\nnumero: %d\n", d.texto, d.numero);
  printf("[ %f, %f, %f ]\n", d.v[0], d.v[1], d.v[2]);
  /* Modificar */
  d.numero = 9999;
}

int main() {
  Datos_t dt, que_se_yo;

  printf("sizeof(Datos_t): %lu", sizeof(Datos_t));

  leer(&dt);
  escribir(dt);
  escribir(dt);

  que_se_yo = dt;
  printf("que se yo ----- \n");
  strcat(que_se_yo.texto, " y esto");
  escribir(que_se_yo);

  printf("dt ----- \n");
  escribir(dt);
  return 0;
}

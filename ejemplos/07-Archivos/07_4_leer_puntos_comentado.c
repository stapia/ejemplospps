#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Puntero a dos doubles es esto:
        -------------------
   p -> | double | double |
        -------------------

   Siendo p+1:

        --------------------------------------
   p -> | double | double || double | double |
        --------------------------------------
                               ^
                               |
                              p+1
*/

/* Definición de un tipo de puntero a un array de 2 doubles */
typedef double (*pPunto2d)[2];

/* Definición de un tipo de puntero a un array de 2 doubles */
typedef double (*pPunto3d)[3];

/* Función para procesar el archivo, separa puntos 2D y 3D */
void procesar(FILE *g, pPunto2d *arrayP2d, size_t *numP2d, pPunto3d *arrayP3d, size_t *numP3d) {
  char linea[256];                /* Almacena cada línea leída del archivo */
  int line_count = 0, leidos = 0; /* Contador de líneas y número de valores leídos */
  size_t N2d = 4;                 /* Capacidad inicial para el array de puntos 2D */
  size_t N3d = 4;                 /* Capacidad inicial para el array de puntos 3D */
  double aux_V[3];                /* Array temporal para almacenar un punto leído */

  /* Reserva memoria para el array de puntos 2D */
  *arrayP2d = calloc(N2d, sizeof(double[2]));
  *numP2d   = 0; /* Inicializa el contador de puntos 2D */

  /* Reserva memoria para el array de puntos 3D */
  *arrayP3d = calloc(N3d, sizeof(double[3]));
  *numP3d   = 0; /* Inicializa el contador de puntos 2D */

  while (fgets(linea, 256, g)) { /* Lee línea por línea del archivo */
    ++line_count;
    /* Intenta leer 2 o 3 valores de la línea */
    leidos = sscanf(linea, "%lf , %lf , %lf", aux_V, aux_V + 1, aux_V + 2);
    if (leidos == 2) {
      /* Si se leen 2 valores, es un punto 2D */
      if (*numP2d + 1 >= N2d) { /* Redimensiona el array si es necesario */
        N2d *= 2;
        *arrayP2d = realloc(*arrayP2d, N2d * sizeof(double[2]));
      }
      (*arrayP2d)[*numP2d][0] = aux_V[0]; /* Almacena el punto en el array */
      (*arrayP2d)[*numP2d][1] = aux_V[1];
      ++(*numP2d); /* Incrementa el contador de puntos 2D */
    } else if (leidos == 3) {
      /* Si se leen 3 valores, es un punto 3D */
      if (*numP3d + 1 >= N3d) { /* Redimensiona el array si es necesario */
        N3d *= 2;
        *arrayP3d = realloc(*arrayP3d, N3d * sizeof(double[3]));
      }
      /* Almacena el punto en el array com memcpy */
      memcpy((*arrayP3d)[*numP3d], aux_V, 3 * sizeof(double));
      ++(*numP3d); /* Incrementa el contador de puntos 2D */
    } else {
      /* Si no se leen 2 o 3 valores, muestra un error */
      fprintf(stderr, "Linea #%d: No se ha podido leer el punto\n", line_count);
    }
  }
}

/* Muestra los puntos 2D */
void mostrar2d(double arrayP2d[][2], size_t numP2d) {
  size_t i;
  for (i = 0; i < numP2d; ++i) {
    printf("P2d: %5.2f %5.2f\n", arrayP2d[i][0], arrayP2d[i][1]);
  }
}

/* Muestra los puntos 3D */
void mostrar3d(double arrayP3d[][3], size_t num) {
  size_t i;
  for (i = 0; i < num; ++i) {
    printf("P3d: %5.2f %5.2f %5.2f\n", arrayP3d[i][0], arrayP3d[i][1], arrayP3d[i][2]);
  }
}

int main(int argc, char const *argv[]) {
  FILE *g;
  if (argc != 2) { /* Verifica que se haya proporcionado el nombre de un archivo */
    fprintf(stderr, "usage: %s <filename>\n", argv[0]);
    return 1;
  }
  /* Intenta abrir el archivo */
  if ((g = fopen(argv[1], "r")) != NULL) {
    pPunto2d arrayP2d; /* Array de puntos 2D (un puntero) */
    size_t numP2d;     /* Número de puntos 2D */
    pPunto3d arrayP3d; /* Array de puntos 3D (un puntero) */
    size_t numP3d;     /* Número de puntos 3D */
    /* Atención se pasan los punteros ¡Por referencia! */
    procesar(g, &arrayP2d, &numP2d, &arrayP3d, &numP3d);
    /* Muestra los puntos. En estas llamadas se pasa ¡Por valor!  */
    mostrar3d(arrayP3d, numP3d);
    mostrar2d(arrayP2d, numP2d);
    /* Libera la memoria de los arrays de puntos*/
    free(arrayP3d);
    free(arrayP2d);
    fclose(g);
  } else {
    /* Error al abrir el archivo */
    fprintf(stderr, "can not open file `%s'\n", argv[1]);
    return 2;
  }
  return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Busca Stringification de macros del preprocesador */
#define xstr(s) str(s)
#define str(s) #s
#define STR_LEN_MAX 31

void procesar(FILE *g) {
  char palabra[STR_LEN_MAX + 1]; /* Atención al +1!! */
  /* Mientras pueda leer una cadena sigue leyendo */
  while (fscanf(g, "%31s", palabra) == 1) {
    printf("palabra: '%s'\n", palabra);
  }
}

/* Leer palabras del archivo concatenado todas ellas
y, a la vez, separandolas por un punto y coma. La
concatenacion se guarda en una cadena dinámica */
char *procesar_concat(FILE *g) {
  char palabra[STR_LEN_MAX + 1];
  char *resultado = malloc((STR_LEN_MAX + 1) * sizeof(char));
  int lenP, lenR;
  int N = STR_LEN_MAX + 1;
  strcpy(resultado, ""); /* Inicializa a cadena vacía */
  /* resultado[0] = '\0'; */
  while (fscanf(g, "%" xstr(STR_LEN_MAX) "s", palabra) == 1) {
    lenR = strlen(resultado);
    lenP = strlen(palabra);
    if (lenP + lenR + 1 /* este e por el ; */ + 1 /* y este por '\0' */ > N) {
      /* Hace resultado más grande */
      char *aux = malloc(2 * N * sizeof(char)); /* Dobla el tamaño */
      /* Copia lo que había en lo nuevo */
      strcpy(aux, resultado);
      /* Libera el antiguo */
      free(resultado);
      /* Actualiza el resultado con la nueva variable dinámica*/
      resultado = aux;
      /* Actualiza el tamaño (que ya es el doble)*/
      N = 2 * N;
      /* La alternativa a todo código anterior es:
      resultado = realloc(resultado, 2*N*sizeof(char));
      */
    }
    /* Concatena la palabra */
    strcat(resultado, palabra);
    /* Y mete un ; detrás. Ojo: NO se puede hacer concat con ';'!! */
    strcat(resultado, ";");
    printf("palabra: '%s'\n", palabra); /* Para comprobar lo que leemos */
  }
  return resultado;
}

int main() {
  FILE *g;
  char *resultado;
  /* Abre el archivo para leer */
  g = fopen("palabras.txt", "r");
  if (g != NULL) { /* Si se ha abierto correctamente */
    /* Se puede probar con `procesar(g);` para ver el contenido */
    resultado = procesar_concat(g); /* Devuelve una cadena dinámica */
    printf("%s\n", resultado);
    free(resultado); /* Hay que liberar */
    fclose(g);
  } else {
    fprintf(stderr, "No se puede abrir\n");
  }

  return 0;
}

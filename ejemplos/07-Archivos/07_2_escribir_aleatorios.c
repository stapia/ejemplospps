#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int N, i;
    double *array;
    FILE *g;

    if (argc != 2 || sscanf(argv[1],"%d", &N) != 1 || N <= 0 ) {
        return 1;
    }
    array = malloc(N*sizeof(double));

    for ( i = 0; i < N; ++i ) {
        /* Entre 0 y 1 */
        array[i] = rand() * 1.0 / RAND_MAX;
    }

    g = fopen("datos.out", "w");
    if ( g != NULL) {
        for ( i = 0; i < N; ++i ) {
            fprintf(g, "%f\n", array[i]);
        }
        fclose(g);
    }
    free(array);
    return 0;
}

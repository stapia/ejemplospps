
# Escribir archivos

## Escribir en el canal de errores (07_1_escribir_stderr)

El programa:

* Toma dos argumentos, los suma y escribe el resultado en
la salida estándar.

* Si alguno de los argumentos no es un número (coma flotante)
escribe un mensaje de error en el canal estándar de errores.

## Escribir números aleatorios (07_2_escribir_aleatorios)

El programa:

* Toma un número entero como argumento,

* Genera un array de ese tamaño con números aleatorios,

* Guarda los números en un archivo de texto de nombre "datos.out"

Si encuentra un error se escribe un mensaje en el canal de errores. 

# Leer archivos

## Leer palabras (07_3_leer_cadenas)

El programa:

* Abre el archivo de texto "palabras.txt" para leer.
* Lee palabras (máximo 31 caracteres) del archivo.
* Concatena todas ellas separandolas con un carácter ';'

## Leer puntos 2D y 3D

El programa:

* Recibe como parámetro el nombre de un archivo (por ejemplo: puntos.txt)
* Lee cada línea del archivo sabiendo que contiene un punto en 2d ó 3d.
* Guarda cada punto 2D en un array dinámico 
* Guarda cada punto 3D en otro array dinámico
* Escribe los punto 2d y luego los 3d.

### Explicaciones al código:

#### Usar typedef

typedef sirve para declarar *alias* de tipos de datos. 

Se usa typedef para simplificar la escritura de tipos. Por ejemplo, en 
la función procesar hay que pasar el puntero a los puntos 2d por *referencia*
(hay que cambiarlo dentro de la función), la declaración sería bastante 
confusa, sin embargo, con el typedef se debería leer un poco mejor.

En este ejemplo, el tipo `pPunto2d` es un *puntero* que apunta a
dos `double`. Serve para tener un array dinámico de puntos en 2d.

Idem para `pPunto3d`.

#### Sobre la declaración de la función procesar

Esta función necesita proporcionar 4 resultados: el puntero al array 
dinámico de puntos 2d, el número de puntos 2d leídos, el puntero al array 
dinámico de puntos 3d y el número de puntos 3d leídos. 

Todos ellos se han puesto como parámetros **por referencia**. 

Se puede comprobar la diferencia respecto de las funciones que 
muestran el contenido de los datos correspondientes (no hace
falta pasar referencias).

### Sobre memcpy

En `procesar` se usa `memcpy` para copiar el punto 3d auxiliar en
su lugar en el array. Básicamente esta función sirve para copiar *bloques* de 
memoria. Suelen utilizarse con frecuencia para copiar arrays 
completos. 

### Sobre realloc

En `procesar` se usa `realloc` para incrementar el tamaño del array
de puntos 2d en un solo paso. Para usar esta función se debe tener
en cuenta que ya se debe hacer usado alloc (o similares) para 
crear el array dinámico y que el resultado debe ser liberado 
con free. Como ventaja, realloc se encarga de copiar lo que 
ya teníamos guardado y de hacer el free del array *antiguo*.

### Sobre fgets

Cuando hay que leer un archivo por líneas (es decir, cuando el final de línea es un
separador importante en el contenido del archivo), es muy útil la función `fgets`.

Esta función lee una línea (hasta el tamaño indicado) del archivo dado y la 
guarda en el *buffer* dado. A tener en cuenta:

* Retorna la dirección del mismo *buffer* o NULL si no se puede leer.
* Deja el salto de línea en el *buffer*.
* Nunca desborda el *buffer*, si la línea es más larga deja de leer. 

Una vez procesada la línea y almacenada en un *string* se procesa esa cadena. 
Alternativas: sscanf, strtok, de caracter en caracter... Dependiendo del formato
de cada línea. 

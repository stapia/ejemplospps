#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
  int N, i; /* Declaración de un entero N para el tamaño del array y un índice i para los bucles */
  double *array; /* Declaración de un puntero para un array dinámico de doubles */
  FILE *g;       /* Declaración de un puntero para manejar el archivo */

  /* Verifica que se pase un argumento, que sea un número entero positivo
  ** Se recuerda que scanf devuelve el número de datos procesados correctamente */
  if (argc != 2 || sscanf(argv[1], "%d", &N) != 1 || N <= 0) {
    return 1; /* Sale del programa con un código de error si la validación falla */
  }

  /* Reserva memoria para un array de N elementos tipo double */
  array = malloc(N * sizeof(double));

  /* Genera N números aleatorios entre 0 y 1 y los almacena en el array */
  for (i = 0; i < N; ++i) {
    /* Entre 0 y 1 */
    array[i] = rand() * 1.0 / RAND_MAX; /* rand() genera un número entero aleatorio; se normaliza
                                           dividiéndolo entre RAND_MAX */
  }

  /* Abre un archivo llamado "datos.out" en modo escritura, se retorna null si no se puede */
  g = fopen("datos.out", "w");
  if (g != NULL) { /* Verifica que el archivo se abrió correctamente */
    /* Escribe los valores del array en el archivo, uno por línea */
    for (i = 0; i < N; ++i) {
      /* fprintf es igual que printf, sólo cambia el primer argumento */
      fprintf(g, "%f\n", array[i]);
    }
    /* Si se puedo abrir, se cierra el archivo después de escribir */
    fclose(g);
  }
  /* Libera la memoria reservada para el array */
  free(array);
  return 0;
}


#include <stdio.h>

int main(int argc, char const *argv[]) {
  double x, y, suma;

  if (argc != 3) {
    fprintf(stderr, "Hay que poner 2 números\n");
    return 1;
  }

  if (sscanf(argv[1], "%lf", &x) != 1) {
    fprintf(stderr, "Hay que poner un número como primer arg\n");
    return 2;
  }

  if (sscanf(argv[2], "%lf", &y) != 1) {
    fprintf(stderr, "Hay que poner un número como segundo arg\n");
    return 3;
  }

  suma = x + y;
  fprintf(stdout, "La suma es %f\n", suma);

  return 0;
}

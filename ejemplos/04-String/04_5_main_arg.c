
#include <stdio.h>

/*
  args es un array de punteros. Cada puntero apunta a una string.
  La reserva de memoria de esta variable es responsabilidad del
  sistema.
*/
int main(int argc, char* args[]) {
  int i;
  for (i = 0; i < argc; ++i) {
    /* Es conveniente usar un delimitador al escribir strings, por ejemplo ' */
    printf("Arg %2d: '%s'\n", i, args[i]);
  }
  if (argc <= 1) {
    return 2;
  }
  return 0;
}

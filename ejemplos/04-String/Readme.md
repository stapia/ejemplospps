
# Manipulación de cadenas alfanuméricas

## Entrada y salida de cadenas alfanuméricas: 04_1_str_scanf.c

### Explicación

El programa:

* Lee una cadena alfanumérica y la escribe.
* Lo hace de forma **segura** porque el scanf limita los caracteres a leer.
  Es decir, *no* se puede desbordar la cadena.
* Ejecute el programa y redireccione la salida a un archivo, compruebe el 
  tamaño del archivo. 
* Ejecute el programa de la misma manera, pero introduciendo una tilde (o eñe) 
  en la palabra introducida. Vuelva a comprobar el tamaño y explique porqué 
  ocurre.
* Nota: como se puede observar scanf con "%s" lea un *palabra*, es decir, lee 
  hasta el primer blanco.
* Consulta por la Web el significado de UTF-8.

## Otras funciones de string.h (strcat y strcpy)

### Explicación

El programa:

* Lee una palabra y forma una cadena que sea "Hola " y concatenada la palabra 
  introducida.
* Nota: En el código aparecen unos comentarios con la longitud máxima ¿Por qué?
* Modifique el programa para que la cadena que se forme empiece por 
  "Buenas tardes " o "Buenos días " dependiendo si la palabra leída tiene 
  longitud par o impar.
* Nota: ¿Por qué se el comentario en la línea dice que eso está mal?

## Otras funciones de string.h (strcmp)

### Explicación:

El programa:

* Intenta leer una cadena y un número.
* Mientras que la lectura tenga éxito:
* Si la palabra leída es "suma" se debe sumar el valor a un total
* Y si es "multiplicar" se debe multiplicar por el total. 
* Nota: Consulta qué significa el retorno de scanf
* Nota: Consulta cómo terminar la entrada estándar para un programa 
  en ejecución.

## Manipulación carácter a carácter (04_4_str_char.c)

### Explicación

El programa:

* Lee una cadena de la entrada estándar,
* Cuenta los caracteres que son digitos y
* Lo escribe en el canal de salida.
* Nota: El bucle funciona porque *aux es cero al llegar a la
  letra nula (la que marca la terminación de la string)
* Se recuerda: las cadenas en C son **null terminated strings**.

## Uso de los argumentos de `main` (04_5_main_arg.c)

### Explicación:

El programa:

* Hace la defición completa de main (con sus dos parámetros estándar).
* Tiene un bucle para mostrar todos los argumentos que se pasan a main.
* Comprueba si el número de argumentos es menor o igual que 1 y en ese
  caso retorna 2. Retorna 0 en cualquier otro caso. 
* Haga varias ejecuciones para comprobar lo que se muestra en
  el canal de salida y el resultado del programa. (`echo $?`)

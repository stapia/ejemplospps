
#include <stdio.h>
#include <string.h>

#define TAM 32

/* Multiplica o suma caracteres leídos de la entrada */
int main() {
  char cad[TAM];
  int numero, total = 0;

  /* Consulta que significa el retorno de scanf*/
  while (scanf("%31s %d", cad, &numero) == 2) {
    /* MUY MAL: if ( cad == "suma") { */
    if (strcmp(cad, "suma") == 0) {
      total += numero;
    } else if (strcmp(cad, "producto") == 0) {
      total *= numero;
    } else {
      printf("Palabra incorrecta\n");
    }
  }
  printf("Total: %d\n", total);
  return 0;
}

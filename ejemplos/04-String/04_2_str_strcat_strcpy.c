
#include <stdio.h>
#include <string.h>

#define TAM 32

int main() {
  char resultado[TAM] = "Hola ";
  char cad[TAM];

  scanf("%7s", cad);
  strcat(resultado, cad); /* Resultado tiene longitud máxima 12 */
  printf("Resultado es: %s\n", resultado);

  if (strlen(cad) % 2 == 0) {
    /* MAL: resultado = "Buenas tardes"; */
    strcpy(resultado, "Buenas tardes ");
  } else {
    strcpy(resultado, "Buenos días ");
  }
  strcat(resultado, cad); /* Longitud máxima 14 + 7 */
  printf("Resultado es: %s\n", resultado);

  return 0;
}

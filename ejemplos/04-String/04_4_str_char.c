
#include <stdio.h>

#define TAM 32

/* Multiplica o suma caracteres leídos de la entrada */
int main() {
  char cad[TAM];
  char *aux    = cad;
  int contador = 0;

  scanf("%31s", cad);
  while (*aux) {
    if ('0' <= *aux && *aux <= '9') {
      ++contador;
    }
    ++aux;
  }
  printf("Hay %d digitos\n");
  return 0;
}

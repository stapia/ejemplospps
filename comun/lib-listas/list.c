
#include <stdio.h>
#include <stdlib.h>

#include "list.h"

lista_t *crear_vacia()
{
  lista_t *nli = malloc(sizeof(lista_t));
  nli->primero = NULL;
  nli->tam = 0;
  return nli;
}

lista_t *crear(int *v, size_t size)
{
  size_t i;
  lista_t *nli;
  nodo_t *aux_ulti;

  if (size == 0)
  {
    fprintf(stderr, "Al menos tiene que haber un elemento\n");
    return NULL;
  }

  /* Crea el primer nodo (es un caso especial, no se puede
  meter en el bucle)*/
  nli = malloc(sizeof(lista_t));
  nli->primero = malloc(sizeof(nodo_t));
  nli->primero->dato = v[0];
  nli->primero->sig = NULL;
  aux_ulti = nli->primero;

  /* El resto de nodos en un bucle */
  for (i = 1; i < size; ++i)
  {
    aux_ulti->sig = malloc(sizeof(nodo_t));
    aux_ulti = aux_ulti->sig;
    aux_ulti->dato = v[i];
    aux_ulti->sig = NULL;
  }
  nli->tam = size;
  return nli;
}

void destruir(lista_t *li)
{
  nodo_t *aux;
  while (li->primero) /* Mientras que no sea NULL */
  {
    aux = li->primero;
    li->primero = li->primero->sig;
    free(aux);
  }
  free(li);
}

void print(const lista_t *li)
{
  nodo_t *aux = li->primero;
  printf("list: #%ld (", li->tam);
  while (aux != NULL)
  {
    printf(" %d", aux->dato);
    aux = aux->sig;
  }
  printf(" )\n");
}

void insertar(lista_t *li, size_t index, int x)
{
  if (index > li->tam)
  {
    fprintf(stderr, "No se puede insertar más allá del final\n");
    return;
  }
  li->tam += 1;

  if (index == 0)
  { /* Delante del primero, es un caso especial no hay un elemento _anterior_ */
    nodo_t *nn = malloc(sizeof(nodo_t));
    nn->dato = x;
    nn->sig = li->primero;
    li->primero = nn;
  }
  else
  { /* En el medio o al final. Se utiliza el nodo _anterior_ para insertar *detrás* */
    nodo_t *nn;
    nodo_t *anterior = li->primero;
    --index;
    while (anterior->sig != NULL && index > 0) /* anterior->sig != NULL es redundante, por si acaso */
    { 
      anterior = anterior->sig;
      --index;
    }
    nn = malloc(sizeof(nodo_t));
    nn->dato = x;
    nn->sig = anterior->sig;
    anterior->sig = nn;
  }
}

int eliminar(lista_t *li, int dato)
{
  int result = 0;
  if (li->primero->dato == dato)
  { /* Eliminar el primero, es un caso especial no hay un elemento _anterior_ */
    nodo_t *aux = li->primero;
    li->primero = aux->sig;
    free(aux);
    li->tam--;
    result = 1;
  }
  else
  { /* Para el resto hay un nodo _anterior_ que se usa para eliminar el que está *detrás* */
    nodo_t *anterior = li->primero;
    /* Termina cuando se encuentra o cuando llega al último */
    while (anterior->sig != NULL && anterior->sig->dato != dato) 
    {
      anterior = anterior->sig;
    }
    /* Esta condición equivale a decir si ("No es el último") */
    if (anterior->sig != NULL) 
    { /* Se ha encontrado y por tanto se elimina */
      nodo_t *aux = anterior->sig;
      anterior->sig = aux->sig;
      free(aux);
      li->tam--;
      result = 1;
    }
  }
  return result;
}
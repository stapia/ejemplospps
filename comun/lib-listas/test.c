
#include <stdio.h>

#include "list.h"

void test_1() 
{
    int v[] = { 1, 2, 3, 4 };
    lista_t *list;
    printf("---- Test 1 ----\n");
    list = crear(v, 4);
    print(list);
    destruir(list);
}

void test_2()
{
    lista_t *list;
    printf("---- Test 2 ----\n");
    list = crear_vacia();
    insertar(list, 0, -4);
    insertar(list, 0, 3);
    print(list);
    insertar(list, 1, 8);
    insertar(list, 1, 9);
    print(list);
    insertar(list, 4, 13);
    insertar(list, 5, 34);
    print(list);
    eliminar(list, 3);
    print(list);
    eliminar(list, -4);
    print(list);
    eliminar(list, 34);
    eliminar(list, 88);
    print(list);
    destruir(list);
}

int main()
{
    test_1();
    test_2();

    return 0;
}

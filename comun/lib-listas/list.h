
#ifndef LISTA_H_498124RR2989AQWRQH
#define LISTA_H_498124RR2989AQWRQH

/* Define el nodo para crear la lista *enlazada* */
struct nodo
{
    int dato;
    struct nodo *sig;
};

/* Alias del nodo para no tener que poner struct */
typedef struct nodo nodo_t;

/* Define una lista, se usa para *evitar* tener que 
pasar referencias a un nodo en las funciones donde
hay que modificar el primer nodo de la lista. 
(se traduce en un puntero a puntero un poco complicado
de manejar) */
struct lista
{
    nodo_t *primero;
    size_t tam;
};

/* Alias de la lista para no tener que poner struct */
typedef struct lista lista_t;

/* Crea una lista vacia y la retorna */
lista_t *crear_vacia();

/* Crea una lista con los elementos de un array */
lista_t *crear(int *v, size_t size);

/* Elimina todos los nodos de una lista y la 
propia lista. Elimina <-> Libera memoria */
void destruir(lista_t *);

/* Escribe los nodos de la lista en el canal de 
salida estándar. */
void print(const lista_t *li);

/* Inserta un nodo en la posición index con el 
dato dado. La primera posición es la 0, la última
es el tamaño de la lista */
void insertar(lista_t *li, size_t index, int dato);

/* Elimina el primer nodo con el dato dado de la
lista y retorna 1. Retorna 0 y no hace nada si no
se encuentra el dato en la lista. Elimina <-> Libera memoria */
int eliminar(lista_t *li, int dato);

#endif /* fin de #ifndef LISTA_H_498124RR2989AQWRQH */

#include <stdio.h>

#include "list.h"

/* Suma los elementos de una lista */
int main()
{
    int v[5] = { 2, 43, 3, -34, 5 };
    lista_t *list = crear(v, 5);
    /* Podemos usar el nodo_t sin problema para implementar 
    funcionalidades complementerias a las que ya hay */
    nodo_t *aux = list->primero;
    int suma = 0;
    while ( aux != NULL ) {
        suma += aux->dato;
        aux = aux->sig;
    }
    printf("La suma de ");
    print(list);
    printf("es %d\n", suma);
    destruir(list);
    return 0;
}

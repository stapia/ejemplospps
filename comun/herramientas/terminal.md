# Terminal

Una terminal es una ventana + un interprete de comandos (CLI, Command
Line Interface), vamos a usar la terminal por defecto en XUbuntu y 
bash como interprete de comandos. 

### Algunas definiciones

* Comando -> Un programa que está diseñado para ser ejecutado en una 
terminal.

* Argumentos del comando -> información extra que se pasa 
a un comando para modificar su comportamiento o suministrarle 
información. Normalmente se distingue entre opciones (*options*)
que se prefijan con un o dos carácteres guión (`-`) y argumentos
propiamente dichos que no llevan nada. 

* Prompt -> Marca que consiste en una cadena de caracteres y que se 
muestran en la terminal cada vez que finaliza la ejecución de un 
comando (o al arrancar la terminal). El prompt puede incluir diversa
información (por ejemplo: la carpeta de trabajo) y suele acabar en 
caracter especifico de cada terminal (Por ejemplo: "$", "#", ">", 
etc.).

### Configurar el prompt de la terminal

Como un pequeño ejercicio vamos a cambiar el prompt de la terminal
que vamos a utilizar para incluir un salto de línea en el prompt de
manera que se vea la misma información que teníamos antes, pero 
también se vea mejor el comando que vamos a escribir y el caracter
que se usa como fin del prompt. 

Para ello:

* Abrimos el archivo de configuración (.bashrc):

---

```bash
nano .bashrc
```

---

Luego buscamos un par de líneas (al final) que definen la variable PS1
e insertamos \n justo delante del \$.

Grabamos, abrimos otra terminal y observamos el resultado. 

### Otras configuraciones

También podemos tocar otras características de la ventana. Por ejemplo,
puede ser conveniente cambiar el tamaño de la letra...

### Otros conceptos básicos

Al trabajar con la terminal es muy conveniente conocer estos conceptos 
básicos:

* Ruta (de un archivo): El nombre cualificado de un archivo, es decir,
el nombre del archivo precedido por los nombres de las carpeta en que
se encuentra.

* Ruta Absoluta (de un archivo): La ruta indicada desde la carpeta
raíz (`/`) o desde el dispositivo (en Windows, por ejemplo: `C:\`).

* Ruta Relativa (de un archivo): la ruta indicada desde otro punto
del sistema de archivos que no sea la raíz.

* Carpeta de trabajo (working directory): Cuando se usa la 
terminal, a la hora de escribir una ruta relativa se supone que
está escrita desde esta carpeta. 

* Carpeta o archivo oculto. Una carpeta o archivo que no
se muestra por defecto al usuario. Para hacer que una carpeta
o archivo sea oculto se prefija el nombre con un punto (`.`)

* Un punto (`.`): Es la manera de indicar la carpeta de 
trabajo como parte de la ruta y, normalmente, sirve para 
distinguir que se está indicando una ruta relativa. 

* Punto Punto (`..`): Es la manera de indicar la carpeta de 
inmediatamente superior a la última indicada en la ruta
(carpeta padre). 

* Virgulilla (`~`): Es una manera abreviada de indicar
la carpeta de usuario del usuario activo (carpeta *home*).

Ejemplos (Rutas relativas indicadas desde la carpeta
de trabajo):

| Ruta absoluta           | Carpeta de Trabajo      | Ruta Relativa |
|-------------------------|-------------------------|---------------|
| `/home/juana/datos.txt` | `/home/juana`           | `datos.txt` |
| `/home/juana/datos.txt` | `/home/juana`           | `~/datos.txt` |
| `/home/juana/datos.txt` | `/usr/bin`              | `~/datos.txt` |
| `/home/juana/datos.txt` | `/home/juana`           | `./datos.txt` |
| `/home/juana/datos.txt` | `/home/juana/bin`       | `../datos.txt` |
| `/home/juana/data/datos.txt` | `/home/juana/bin`  | `../data/datos.txt` |
| `/home/juana/datos.txt` | `/home/juana/bin`       | `../datos.txt` |
| `/home/juana/datos.txt` | `/home/juana/bin/pack`  | `../../datos.txt` |

### Comandos básicos

* `cd`: cambia la carpeta de trabajo hay que indicar la nueva carpeta
como argumento
* `mv`: mueve o renombra un archivo
* `cp`: copiar un archivo
* `ls`: muestra el contenido de la carpeta de trabajo. Un par de opciones
interesantes son `-l` para mostrar el contenido en formato largo y `-a`
para mostrar archivos ocultos. Por ejemplo: `ls -la`.

### Un par de funcionalidad MUY útiles

1. Autocompletar: la terminal es capaz de autocompletar rutas de archivos
y también algunos argumentos de comandos. Para emplear este autocompletar
se emplea la tecla tabulador (tecla con dos flechas encima del mays). 
1. Historia: la terminal guarda los comando que se han ejecutado 
anteriormente, para acceder y moverse por la historia se usan la flechas
de cursor arriba y abajo. 

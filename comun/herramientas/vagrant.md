
# Vagrant

### ¿Qué es?

Una aplicación para gestionar máquinas virtuales. 

Básicamente permite:

1. Descargar imagenes predefinidas de máquinas virtuales.
1. Arrancarlas fácilmente con una configuración determinada.
1. Añadir particularidades a la máquina de forma automatizada.
1. (...)

### ¿Para qué se va a usar?

Fundamentalmente se va a utilizar Vagrant porque es una manera
*sencilla* de que todos tengáis la máquina virtual sin que haya
que repetir un proceso de instalación que puede ser tedioso. 

Adicionalmente, dado que el objetivo es *cacharrear*, Vagrant
sirve para recuperar de forma sencilla un estado *inicial* de
la máquina, incluso si nos hemos cargado lo que teníamos.

### Instalación

Descargar el instalador e instalar siguiendo las instrucciones
de la página oficial. 

### USO

#### Creación de la VM (máquina virtual)

Vagrant permite crear VM a partir de una VM empaquetada 
que se denomina *box*. El proceso consiste en:

1. Seleccionar la *box*
1. Crear una carpeta para guardar las especificaciones
particulares que queramos dar a la *box*
1. Preparar (descargar y *provisionar*) la *box*

Estos pasos están perfectamente documentados en:

https://learn.hashicorp.com/collections/vagrant/getting-started

No obstante, en resumen lo que será necesario será ejecutar
(en una carpeta local vacia del ordenador que se vaya a utilizar):

---

```bash
vagrant init stapia/xubuntu_21_04_es
```

---

En este paso se descarga la imagen inicial y se genera un 
archivo `Vagrantfile` con las opciones *por defecto* para 
la VM. 

Solo vamos a tocar *discrecionalmente* dos opciones en la 
seccion `config.vm.provider "virtualbox" do |vb|`: 

* Descomentar el inicio y final de la sección (cambiar):
para ello hay que quitar los caracteres almohadilla (`#`) 
del comienzo de sección (`config.vm.provider "virtualbox" do |vb|`)
y del final de la sección (el `end` unas líneas más abajo).
* Le ponemos una CPU adicional (añadir):
```
vb.cpus = "2"
```
* Le ponemos más memoria (cambiar):
```
vb.memory = "2048"
```
* Si no está puesto ponemos que se muestre la interfaz
gráfica de la VM (comprobar, quizás descomentar o añadir):
```
vb.gui = true
```

En este archivo podríamos poner algún paso extra para 
preparar la VM de manera automática (lo que se llama
*provisionamiento*), pero el resto de pasos los vamos
a hacer más o menos a mano para ir *cachareando*.

Un vez configurada ya la podemos *arrancar*, se 

---

```bash
vagrant up
```

---

Al arrancar es cuando realmente se genera la VM en 
Virtual Box y, a su vez, se arranca. Para detener
la VM lo más directo es *apagarla* directamente en 
el sistema simulado (el *guest*). 

#### Sobre la *box*

La box: `stapia/xubuntu_21_04_es` es una versión core de 
XUbuntu 21.04 in

#### Uso sucesivas veces

El comando: `vagrant up` ejecutado en la carpeta donde se
creo la máquina arrancará de nuevo la VM. 

Es importante saber que:

* La VM no está congelada, es decir, los cambios que hagamos
en la máquina, tanto aplicaciones instaladas como archivos
personales, son persistentes y se mantendrán la sucesivas 
veces que arranquemos la VM.
* Se puede volver a empezar en cualquier momento... Lo que
implica naturalmente perder todos los cambios, tanto
de aplicaciones como de archivos personales que hayamos
introducido (¡Cuidado!). 
* Se puede compartir información entre la VM y el host
mediante una carpeta compartida automáticamente por Vagrant,
esta carpeta es justo donde hemos generado y arrancado
la VM y se *monta* en la VM en la carpeta `/vagrant`.

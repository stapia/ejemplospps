
# Git

### Qué es

Git es un gestor de versiones. O dicho de otra manera: es un programa
que permite guardar el historico de cambios de un archivo, los puntos
de la historia del archivo que se guardan son las *versiones*.

### Para instalar

En la VM que vamos a usar no está git por defecto, de manera que lo 
vamos a instalar desde la terminal, hacemos:

---

```bash
sudo apt-get install git
```

---

Y confirmamos cuando nos lo pida.

**Importante:** antes de instalar en la máquina virtual es posible
que sea obligatorio (al menos es conveniente) actualizar los 
*origenes de software*, para ello hay que hacer:

---

```bash
sudo apt-get update
```

---

Una vez hecho este paso no es necesario repetir en sucesivas 
instalaciones salvo que hay pasado un periodo largo de tiempo.



### Para bajar el repositorio 

Para *clonar*, es decir, para empezar a trabajar con este repositorio
hacemos: 

---

```bash
git clone https://stapia@bitbucket.org/stapia/ejemplospps.git
```

---

Este paso solo hay que hacerlo una vez, git nos bajará el repositorio
con todas sus versiones y a partir de ese momento lo tendremos
disponible localmente (en nuestro propio ordenador).

Para *actualizar*, es decir, para bajar las nuevas versiones que
se hayan subido del repositorio y mezclarlas con la información
que tenemos localmente hay que hacer (en la carpeta local
del repositorio):

---

```bash
git pull
```

---

### Modificar el repositorio

Como parte del trabajo en la asignatura el alumno debería modificar
los ejemplos para hacer sus propias pruebas. Para poder hacer
estos cambios de manera sencilla y poder seguir incorporando 
los nuevos ejemplos que se van a ir haciendo, una forma de 
trabajo, más o menos sencilla, es utilizar ramas (*branches*)
creadas localmente en el ordenador de propio alumno. 

El proceso sería el siguiente:

1. Crear una rama (local)
```bash
git branch mi-rama-1
```
1. Cambiar de rama activa
```bash
git checkout mi-rama-1
```
1. Generar una nueva versión (guardar cambios en la rama)
```bash
git add *
git commit -m"Comentario de los cambios"
```
Es importante hacer este paso cada vez que se quieran
guardar cambios **y siempre** antes de cambiar de rama. 
Este paso se puede abreviar en un único comando así:
```bash
git commit -am"Comentario de los cambios"
```

Donde `mi-rama-1` es el identificador de la rama que se va a
crear/usar. Se pueden crear tantas ramas como se desee. 

Normalmente se repetiran los pasos a discreción. Para volver a 
la rama principal se hace: `git checkout master`

Nota **Importante**: al cambiar de rama (`checkout`) se modifican
todos los archivos a la última versión en esa rama. Esto no significa
que los cambios se pierdan, todas las versión estás guardadas
y, por tanto, un nuevo cambio de rama volverá a cambiar los 
archivos a la última versión en esa rama. 


# Geany

### ¿Qué es?

Un pequeño IDE, prácticamente un editor de texto con unas pocas funcionalidades
adicionales, entre ellas un par de botones para compilar y ejecutar cómodamente.

### Precaución de uso en la VM

***Importante:*** Geany da un error respecto de que no puede guardar los 
archivos en la carpeta compartida. Al parecer es un error que aparece con
Virtual Box y que se puede arreglar *fácilmente* cambiando una
opción en las preferencias de Geany.

La solución se puede encontrar en:

https://qastack.mx/unix/62782/gedit-cannot-save-in-shared-folder-virtualbox

Hay que:

1. Arrancar Geany
1. Ir a Menu Editar -> Menu Preferencias -> Solapa Varias
1. Localizar y activar la opción: `files.use_atomic_file_saving`
1. Localizar y desactivar la opción: `files.use_gio_unsafe_file_saving`

Nota: Por lo que a mi respecta esta solución solo funciona 
parcialmente, es decir, me sale otro error respecto del renombrado
de un archivo de forma intermitente. 

### Configurar con tema oscuro

Normalmente el tema oscuro es más agradable a la vista, vamos a 
configurar Geany con tema oscuro. 

Seguir las instrucciones de la Web... Para practicar un poco
con la terminal se recomienda utilizarla para comprobar si
existe la carpeta y mover el archivo. 

https://www.geany.org/download/themes/

El tema se puede escoger al gusto de cada quien, por mi parte 
he elegido *Monokai*

Para que el resto de ventanas de Geany también salgan con 
tema oscuro se puede cambiar el tema de XUbuntu a oscuro y 
Geany lo utilizará. 

Para ello hay que ir al *Administrador de Configuración* y 
cambiar el tema en la *Apariencia* y en el *Gestor de Ventanas*.

### Problema con el guión bajo

Otro pequeño problema en Geany (no es directamente culpa suya) 
es que no se ve correctamente el guión bajo en la configuración
por defecto. El problema está en la fuente, la solución más
sencilla es **cambiar la fuente de tamaño 10 a tamaño 12** y con eso
se arregla. 

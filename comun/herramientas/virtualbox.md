
# Virtual Box

### ¿Qué es?

Una aplicación para *virtualizar* un ordenador. 

Es decir, una aplicación (un programa) que permite *simular* un ordenador físico
incluyendo, especialmente, su Sistema Operativo, y cuya utilidad normalmente
está relacionada con la capacidad de administrar la máquina de forma aislada e
independiente de la máquina física. 

#### Algunos conceptos (básicos)

* Host -> la máquina donde se corre la aplicación que virtualiza
* Guest -> la máquina simulada que corre dentro de la aplicación de virtualización

### ¿Para qué usar Virtual Box?

Fundamentalmente para unificar el entorno de trabajo/desarrollo mediante el 
uso de una máquina virtual *única*, es decir, será como trabajar todos en
un ordenador formalmente idéntico.

### Instalar

Directamente descargar el instalador y seguir las instrucciones de instalación
de la página oficial de Virtual Box. 

### Uso

Realmente no vamos a usar Virtual Box sino a través de Vagrant, no obstante,
una vez *generada* la máquina virtual con Vagrant si se podrá arrancarla
y usarla directamente desde el propio Virtual Box. 



# Compilador GCC

Es el compilador de C, lo vamos a usar desde Geany o desde la terminal
mediante el comando:

---

```bash
gcc -ansi -pedantic -Wall -Wextra programa.c -o programa.exe
```

---

Donde *programa.c* es el archivo con el código fuente y *programa.exe*
será el programa compilado (y ejecutable).

**Nota**: Normalmente los programas en sistemas POSIX no llevan extensión,
se la vamos a poner para poner el programa en la misma carpeta que
el código fuente y poder ignorar los archivos ejecutables en git. Más 
adelante podremos todos los programas compilados en una carpeta aparte e 
ignoraremos la carpeta completa. 

Para ejecutar el programa desde la terminal hay que hacer:

---

```bash
./programa.exe
```

---

Nota: Por defecto, la terminal **NO** busca programas o comandos
en la carpeta de trabajo, por eso hay que forzar a que lo haga
poniendo el punto inicial que significa, precisamente, carpeta
de trabajo.


/* Comentario */
/* La // no existe como marca de comentario */

#include <stdio.h>

int main() 
{
	int num = 22, otro = 33;
	char operacion;
	
	scanf("%d%d %c",&num, &otro, &operacion);
	
	printf("El doble es: %d\n", 2*num);
	
	if ( operacion == '*' ) {
		printf("El producto es: %d\n", num * otro);
	} else if ( operacion == '+' ) {
		printf("La suma es: %d\n", num + otro);
	} else {
		printf("Error: '%c' no es correcta\n", operacion);
	}
	
	scanf("%1d%1d",&num, &otro);
	printf("num: %d, otro: %d\n", num, otro);
	
	return 0;
}

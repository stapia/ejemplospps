
# Salida estándar

## Escribir un programa que:

* Escriba en el canal de salida estándar el resultado de los siguientes
ítems:

	1. El texto "Hola mundo",
	1. el cuadrado de una variable entera,
	1. el resultado de sumar la letra 'a' y 2,
	1. el resultado de dividir 10 entre 3,
	1. el resultado de dividir 10 entre 3.0,
	1. el resultado de dividir 1 entre 0.0.

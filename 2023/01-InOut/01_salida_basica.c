
#include <stdio.h>

int main() {
	int numero = 3; /* Inicializacion */
	int cuadrado; /* declaracion sin inializar */
	int a; 
	
	double dividir = 10 / 3;
	printf("10 / 3 es %f\n", dividir);
	
	dividir = 10 / 3.0;
	printf("10 / 3.0 es %f\n", dividir);
	
	dividir = 1 / 0.0;
	printf("1 / 0.0 es %f\n", dividir);
	
	a = 'a' + 2;
	printf(" 'a' + 2 es %d\n", a);
	printf(" 'a' + 2 es %c\n", a);
	
	cuadrado = numero * numero; /* Asignacion */
	
	printf("El cuadrado es %d\n", cuadrado);
	
	printf("Hola mundo\n");
	return 0;
}

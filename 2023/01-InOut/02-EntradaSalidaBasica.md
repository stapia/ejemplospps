
# Entrada Salida estándar

## Escribir un programa que:

* Lea del canal de entrada un número entero y escriba su doble
* Modifique el mismo programa para que multiple dos números leídos 
del canal de entrada
* Modifique el mismo programa para que, adicionalmente, lea un
caracter que podrá ser un '+' o un '*' y entonces escriba
en el canal de salida repectivamente la suma o el producto de los
números
* Cree un nuevo programa a partir de este último para que 
haga lo mismo con números en coma flotante (double).

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main_with_static_array() {
  char str[120];
  char *arrayStr[3];

  while (fgets(str, 120, stdin)) {
    int i = 0, j = 0;
    char *pch;
    pch = strtok(str, " ,.-A\n");
    while (pch != NULL && i < 3) {
      printf("%s\n", pch);
      arrayStr[i] = pch;
      pch         = strtok(NULL, " ,.-A\n");
      ++i;
    }
    printf("En orden inverso: \n");
    for (j = i - 1; j >= 0; --j) {
      printf("%s\n", arrayStr[j]);
    }
  }
  return 0;
}

int main() {
  char str[120];
  char **arrayStr;
  size_t tam = 8;
  arrayStr   = calloc(tam, sizeof(char *));

  while (fgets(str, 120, stdin)) {
    size_t i = 0, j;
    char *pch;
    pch = strtok(str, " ,.-A\n");
    while (pch != NULL) {
      printf("%s\n", pch);
      if (i > tam) {
        tam *= 2;
        arrayStr = realloc(arrayStr, tam * sizeof(char *));
      }
      arrayStr[i] = pch;
      pch         = strtok(NULL, " ,.-A\n");
      ++i;
    }
    printf("En orden inverso: \n");
    for (j = i; j > 0; --j) {
      printf("%s\n", arrayStr[j - 1]);
    }
  }
  free(arrayStr);
  return 0;
}
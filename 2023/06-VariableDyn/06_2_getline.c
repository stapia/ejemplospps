
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char *line = NULL;
    size_t n = 0;
    ssize_t nread;

    nread = getline(&line, &n, stdin);
    while ( nread > 0 ) {
        /* Hacer algo con la línea */
        printf("Line (%lu) %s", strlen(line)-1, line);
        /* Seguir leyendo */
        nread = getline(&line, &n, stdin);
    }
    free(line);

    return 0;
}

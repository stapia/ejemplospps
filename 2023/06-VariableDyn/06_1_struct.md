
# Definición y uso de struct

* Definir un tipo `struct`, `Datos`, con: una cadena, un número entero y un array de 3 número en 
  en coma flotante.
* Hacer un alias del tipo (`typedef`) que sea `Datos_t`.
* Declarar una variable de tipo `Datos_t`.
* Escribir una función que lea del canal estándar los datos de una variable `Datos_t`.
* Escribir una función que escriba en el canal de salida estándar los datos de una
  variable de tipo `Datos_t` en formato: `etiqueta : valor` con la cadena entre comillas
  dobles y el vector entre corchetes y con las componentes separadas por comas.
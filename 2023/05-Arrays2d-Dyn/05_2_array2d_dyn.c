
#include <stdio.h>
#include <stdlib.h>

int** leer(int nf, int nc) {
    int i, j;
    int **ap;

    ap = calloc(nf, sizeof(int*));

    for ( i = 0; i < nf; ++i ) {
        ap[i] = calloc(nc, sizeof(int));
        for ( j = 0; j < nc; ++j ) {
            int num;
            scanf("%d", &num);
            ap[i][j] = num;
        }
    }
    return ap;
}

void escribir(int **ap, int nf, int nc) {
    int i, j;
    for ( i = 0; i < nf; ++i ) {
        for ( j = 0; j < nc; ++j ) {
            printf("%4d ", ap[i][j]);
        }
        printf("\n");
    }    
}

void liberar(int **ap, int nf) {
    int i;
    for ( i = 0; i < nf; ++i ) {
        free(ap[i]);
    }
    free(ap);
}

int main()
{
    int **a;
    a = leer(7, 5);
    escribir(a, 7, 5);
    liberar(a, 7);
    return 0;
}

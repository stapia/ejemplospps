
#include <stdio.h>

int main(int argc, char const *argv[])
{
    /*double array[5]; */
    double *array;
    unsigned i, tam;

    (void)argc; (void)argv;

    scanf("%u", &tam);
    /* equivale a array = new double[tam]; en Java */
    array = calloc(tam, sizeof(double));
    for ( i = 0; i < tam; ++i ) {
        scanf("%lf", array + i);
    }
    free(array);

    return 0;
}

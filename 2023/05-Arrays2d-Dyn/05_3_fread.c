
#include <stdio.h>

int main()
{
    int a[] = { 1, 2, 3, 4 };
    FILE *g = fopen("datos.dat", "w");
    if ( g != NULL ) {
        fwrite(a, sizeof(int), 4, g);
        fclose(g);
    }
    a[0] = 222;
    g = fopen("datos.dat", "r");
    if ( g != NULL ) {
        fread(a, sizeof(int), 4, g);
        fclose(g);
    }
    printf("%d", a[0]);
    return 0;
}

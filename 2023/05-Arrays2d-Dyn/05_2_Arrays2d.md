
# Arrays 2D (y dinámicos)

* Escribe un programa que lea del canal de entrada estándar 
  un array de tamaño 7x5 (7 filas, 5 columnas) y la guarde en memoria.
* Escriba una función que escriba la suma de las filas de un array 
  como en el apartado anterior.
* Escriba una función para que se guarde en un archivo binario el 
  array anterior, pero guardando el tamaño en el propio archivo.
* Escriba una función como la guardada en el apartado anterior
  teniendo en cuenta que el tamaño puede ser arbitrario. 
* Escriba una función que escriba la suma de las filas de un array
  obtenida como en el apartado anterior.
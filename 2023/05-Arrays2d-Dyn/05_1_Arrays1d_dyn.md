
# Arrays 1d dinámicos.

* Escriba un programa que lea del canal de entrada un número entero, `n_elem`, 
  y luego lea `n_elem` datos en coma flotante y los guarde en memoria.
* Modifique el programa para que esa lectura se realice en una función.
* Escriba una función que calcule la media del array leído.
* Modifique el programa para que `n_elem` se pase como argumento de `main`.
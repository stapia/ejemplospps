
#include <stdio.h>

int main()
{
    unsigned char a[] = { 65, 66, 67, 68 };
    FILE *g = fopen("datos.dat", "w");
    if ( g != NULL ) {
        fwrite(a, sizeof(unsigned char), 4, g);
        fclose(g);
    }
    a[0] = 222;
    g = fopen("datos.dat", "r");
    if ( g != NULL ) {
        fread(a, sizeof(unsigned char), 4, g);
        fclose(g);
    }
    printf("%d", a[0]);
    return 0;
}


# Funciones (parámetros por valor)

## Escribir un programa que:

* Lea de la entrada estándar dos números enteros y escriba
en la salida el máximo de ambos.
* Escribe un bloque de código fuente que declare dos 
variables cuyos valores se inicialicen a las variables 
enteras y realicen el cálculo del máximo. 
* Modifique el programa para que se repita el proceso con 
otros dos valores (usando el mismo bloque).
* Defina una función que calcule el máximo de dos números
y sustituya el código anterior por las llamadas a la función.

## Valore lo que significa el paso de parámetros a una función.

* Wooclap

# Funciones (parámetros por referencia)

## Escribir un programa que:

* Lea de la entrada estándar dos números enteros y escriba
en la salida los valores ordenados.
* Repita el proceso con otros dos números enteros.
* Defina una función que realice la misma funcionalidad.
* ¿Es posible?

## Usando punteros

Escriba otro programa que: 

1. Declara dos variables enteras y lee sus valores de la entrada estándar.
1. Modifique el programa, introduce un bloque donde se declaren dos punteros,
se inicialicen con las direcciones de las variables anteriores y se ordenen 
los valores de la variables.
1. Repite el proceso con otras dos variables enteras usando el mismo bloque. 

## Parámetros por referencia

* Escriba otro programa que lleve el código fuente a la definición de una función. 
* Escriba una función que ordene 3 variables por su valor (de mayor a menor). 

## Consultar y usar la función frexp

* Revisar la documentación https://cplusplus.com ó https://man7.org/linux/man-pages/man3/frexp.3.html
* Escribir un programa que la llame y escriba los resultados en la salida estándar.
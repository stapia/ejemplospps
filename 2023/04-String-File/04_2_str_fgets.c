
#include <stdio.h>
#include <string.h>

#define TAM 16

int main()
{
    char cad[TAM];
    int len;
    int i;

    fgets(cad, TAM, stdin);
    len = strlen(cad);
    printf("Línea (%d): '%s'\n", len, cad);

    for ( i = 0; cad[i] != '\0'; ++i) {
        printf("%d ", cad[i]);
    }
    printf("\n");

    return 0;
}

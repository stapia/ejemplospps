
# Manipulación de cadenas alfanuméricas

## Entrada y salida de cadenas alfanuméricas (scanf)

* Escribe un programa lea una cadena alfanumérica y muestre la misma cadena que se ha introducido.
* Escriba el mismo programa de forma segura. Es decir, que *no* se pueda desbordar la cadena.
* Ejecute el programa y redireccione la salida a un archivo, compruebe el tamaño del archivo. 
* Ejecute el programa de la misma manera, pero introduciendo una tilde (o eñe) en la palabra introducida. Vuelva a comprobar el tamaño y explique porqué ocurre.
* Nota: como se puede observar scanf con "%s" lea un *palabra*, es decir, lee hasta el primer
  blanco.

## Entrada y salida de cadenas alfanuméricas (fgets)

* Escriba otro programa para lea una línea completa de texto y la escriba.
* ¿Es seguro usar fgets?
* Modifique el programa para que la línea leída sea muy pequeña.
* ¿Se puede saber si la línea se ha leído completamente?
* Modifique el programa para mostrar el número de carácteres en la cadena. 
* Adicionalmente haga que se muestre el valor numérico de cada carácter. Introduzca una línea con
  alguna tilde. ¿Qué se muestra?

## Otras funciones de string.h (strcat y strcpy)

* Escriba otro programa que lea una palabra y forme (no escriba) una cadena 
  que sea "Hola " y luego la palabra introducida.
* ¿Es seguro?
* Modifique el programa para que la cadena que se forme empiece por "Hola " o 
  "Buenos días " dependiendo si la palabra leída tiene longitud par o impar.

## Otras funciones de string.h (strcmp)

* Escriba un programa que, mientras se la lectura sea correcta, lea una palabra 
  y un número entero. Si la palabra leída es "suma" se debe sumar el valor a 
  un total y si es "multiplicar" se debe multiplicar por el total. 

## Manipulación carácter a carácter

* Escriba una función que modifique una cadena de manera que convierta todos
  sus carácteres en mayúsculas. 

## Uso de los argumentos de `main`

* Haga la defición completa de main.
* Escriba un bucle para mostrar todos los argumentos que se pasan a main
  (Ver *comun/programas/10_main_arg.c*).
* Haga varias ejecuciones para comprobar el resultado.
* Modifique el programa para que `main` devuelva 1 si el número de 
  argumentos en menor que 2 (y el 0 habitual en caso contrario).
* Ejecute el programa y compruebe los valores que se retornan.

## Lectura y escritura de archivos *en binario* (Postpuesto)

* Utilice el comando `echo` para generar un archivo de texto que incluya 
  varios números. Por ejemplo: "223 45".
* Escriba un programa que comprueba si tiene un argumento:
   i) si es así, debe utilizar ese argumento como el nombre de un archivo
   y abrirlo para lectura,
   ii) en caso contrario debe leer del canal de entrada estándar.
* El programa debe leer el archivo anterior (o la entrada) en bloques 
  de 4 bytes y escriba sus valores numéricos (fread).
* Escriba otro programa que defina un array de enteros y
  la escriba en forma binaria en la salida estándar. 


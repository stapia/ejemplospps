
#include <stdio.h>

#define DIM 5

int main()
{
    double arr[DIM];
    double media;
    int i;

    for ( i = 0; i < DIM; ++i ) {
        scanf("%lf", arr + i); /* &(arr[i]) */
    }

    media = 0;
    for ( i = 0; i < DIM; ++i ) {
        media += arr[i];
    }
    media /= DIM;
    printf("La media es %8.2f\n", media);

    for ( i = 0; i < DIM; ++i ) {
        if ( arr[i] < 0 ) {
            arr[i] = - arr[i];
        }
    }

    printf("( %.2f", arr[0]);
    for ( i = 1; i < DIM; ++i ) {
        printf(", %.2f", arr[i]);
    }
    printf(" )\n");

    return 0;
}


#include <stdio.h>

void escribir_a_archivo(FILE* g, const double *arr, unsigned tam) {
    unsigned i;
    fprintf(g, "Escribir a archivo\n");
    fprintf(g, "( %.2f", arr[0]);
    for ( i = 1; i < tam; ++i ) {
        fprintf(g, ", %.2f", arr[i]);
    }
    fprintf(g, " )\n"); 
}

void escribir(const double *arr, unsigned tam) {
    FILE *g;
    unsigned i;
    if ( (g = fopen("04-datos.out", "w")) != NULL ) {
        fprintf(g, "( %.2f", arr[0]);
        for ( i = 1; i < tam; ++i ) {
            fprintf(g, ", %.2f", arr[i]);
        }
        fprintf(g, " )\n");
        fclose(g);    
    }
}

int leer(FILE *g, double *arr) {
    int i = 0;
    int ok;
    ok = fscanf(g,"%lf", &(arr[i]));
    while ( ok == 1 && i < 10) { /* tam < 10 */
        ++i;
        ok = fscanf(g,"%lf", &(arr[i]));
    }
    return i;
}

int main()
{
    double arr[5];
    double arr10[10];
    int n_elem;
    FILE *g; 

    g = fopen("01-datos.txt","r");

    if ( g != NULL ) {
        int i;
        for ( i = 0; i < 5; ++i ) {
            fscanf(g,"%lf", &(arr[i])); 
        }
        fclose(g);
        escribir(arr, 5);
        escribir_a_archivo(stdout, arr, 5);
    }

    n_elem = leer(stdin, arr10);
    escribir_a_archivo(stdout, arr10, n_elem);

    return 0;
}

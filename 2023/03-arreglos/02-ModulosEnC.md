
# Cómo dividir un archivo fuente en C.

## Consultar breviario

* Pero se colocan declaraciones en .h
* E implementaciones en .c

## Aplicar esta división al programa anterior

* Coloque la declaración de las funciones en un .h
* La implementación en un .c
* En un archivo `02-main.c` (en `main`) coloque las llamadas a las funciones.
* Preste atención al proceso de compilación.

#include <stdio.h>

#define DIM 5

void leer(double arr[DIM]) {
    int i;
    for ( i = 0; i < DIM; ++i ) {
        scanf("%lf", arr + i); 
    }
    printf("He leido un array de size: %ld\n", sizeof(arr));
}

double calcular_media(const double *arr, int n_elem) {
    double media = 0; int i;
    for ( i = 0; i < n_elem; ++i ) {
        media += arr[i];
    }
    media /= n_elem;
    return media;
}

void valor_absoluto(double arr[], unsigned n_elem) {
    unsigned int i;
    for ( i = 0; i < n_elem; ++i ) {
        if ( arr[i] < 0 ) {
            arr[i] = - arr[i];
        }
    }    
}

void escribir(const double *arr, int n_elem) {
    int i;
    printf("( %.2f", arr[0]);
    for ( i = 1; i < n_elem; ++i ) {
        printf(", %.2f", arr[i]);
    }
    printf(" )\n");
}


# Utilización de Arrays

## Escribe un programa que:

1. Lea una serie de datos en coma flotante de la entrada estándar y
se guarden en un array,
1. Calcule la media de esos datos,
1. La escriba en la salida estándar,
1. Modifique los datos del array de manera que se sustituyan los 
valores negativos por su valor absoluto.  

## Modifique el programa anterior para que:

* Vuelva a calcular la media del array después de modificado.
* ¿Tiene sentido volver a escribir el código fuente que calcula 
la media?
* Rectifique el programa para que las funcinalidad básicas del
programa anterior se implementen a través de funciones.
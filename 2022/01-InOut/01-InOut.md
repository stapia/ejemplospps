
# Entrada / Salida estándar

## Escribir un programa que:

* Lea los siguientes datos del canal de entrada estándar:

1. Un número entero,
1. Un número en coma flotante,
1. Un carácter (letra),

* Escriba en el canal de salida estándar los mismos datos.

* Ahora introducimos algunas variantes:

1. Escriba el entero como si fuese un carácter,
1. Escriba el carácter como si fuese un entero,
1. Escriba el entero como coma flotante y viceversa. 
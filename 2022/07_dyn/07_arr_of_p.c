#include <stdio.h>

int main(int argc, char* argv[])
{
    if ( argc >= 3 ) {
        int numero;
        printf("El argv[2] es '%s'\n", argv[2]);
        if ( sscanf(argv[2], "%d", &numero) == 1 ) {
            printf("es el numero: %d\n", numero);
        }
    }
    if ( argc >= 2 ) {
        printf("El argv[1] es '%s'\n", argv[1]);
    } 
    printf("El programa es '%s'\n", argv[0]);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>

int main()
{
    int filas, cols, i, j;
    double* matriz;
    scanf("%d %d", &filas, &cols);
    matriz = malloc(sizeof(double)*filas*cols);

    for ( i = 0; i < filas; ++i ) {
        for ( j = 0; j < cols; ++j ) {
            double x;
            scanf("%lf", &x);
            matriz[i*cols+j] = x;
        }
    } 

    for ( i = 0; i < filas; ++i ) {
        for ( j = 0; j < cols; ++j ) {
            printf("%f ", matriz[i*cols+j]);
        }
        printf("\n");
    } 
    
    printf("\n");
    free(matriz);
    return 0;
}

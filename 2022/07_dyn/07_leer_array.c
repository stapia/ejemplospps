#include <stdio.h>
#include <stdlib.h>

int main()
{
    int tam, i;
    double* vector;
    scanf("%d", &tam);
    vector = malloc(sizeof(double)*tam);
    for ( i = 0; i < tam; ++i ) {
        scanf("%lf", vector + i);
    } 
    for ( i = 0; i < tam; ++i ) {
        printf("%f ", vector[i]);
    }
    printf("\n");
    free(vector);
    return 0;
}

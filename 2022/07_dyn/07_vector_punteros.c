#include <stdio.h>
#include <stdlib.h>

int main()
{
    int filas = 3, cols, i, j;
    double* matriz[3];
    scanf("%d", &cols);

    for ( i = 0; i < filas; ++i ) {
        matriz[i] = malloc(sizeof(double)*cols);
    }
    
    for ( i = 0; i < filas; ++i ) {
        for ( j = 0; j < cols; ++j ) {
            double x;
            scanf("%lf", &x);
            matriz[i][j] = x;
        }
    } 
    for ( i = 0; i < filas; ++i ) {
        for ( j = 0; j < cols; ++j ) {
            printf("%f ", matriz[i][j]);
        }
        printf("\n");
    } 
    printf("\n");
    for ( i = 0; i < filas; ++i ) {
        free(matriz[i]);
    }
    return 0;
}

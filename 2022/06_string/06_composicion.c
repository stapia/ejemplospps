
#include <stdio.h>
#include <string.h>

/* Construir una string con el contenido
   de otra dada, escribiendo delante "Hola ". 
   La cadena destino debe tener suficiente 
   sitio para guardar el resultado */
void saludar(char str[], const char* dada) {
    strcpy(str, "Hola ");
    strcat(str, dada);
} 

/* Construir una string con dos coordenadas 
   con formato (x, y)*/
void rellenar(char* str, double x, double y) {
    sprintf(str, "(%.2e, %.2e)\n", x, y);
} 

void extraer(char* str, double *px, double *py) {
    sscanf(str, "(%lf,%lf", px, py);
} 

void escribir(const char *str) {
    unsigned int i;
    for ( i = 0; i < strlen(str); ++i ) {
        printf("%c\n", str[i]);
    }
}

void escribirPuntero(const char *str) {
    for ( ; *str; ++str ) {
        printf("%c ", *str);
    }
    printf("\n");
}

int main(/*int argc, char const *argv[]*/)
{
    char str1[256];
    char str2[256];
    double x, y;
    scanf("%255s", str2);
    saludar(str1, str2);
    escribir(str1);
    escribirPuntero(str1);
    printf("%s\n", str1);
    printf("Me salto 3: %s\n", str1 + 3);
    rellenar(str1, 2.3, -2.45e-5);
    printf("%s\n", str1);
    extraer(str1, &x, &y);
    printf("x = %.14f y = %.14f\n", x, y);
    /* extraer("(8.5, 7.4)", &x, &y); No está del todo bien*/
    return 0;
}


#include <stdio.h>
#include <string.h>
#define TAM 8

int main()
{
    char cadena[TAM];
    char otra[] = "Hola Mundo ";

    scanf("%7s", cadena);
    printf("Cadena: '%s' strlen: %ld\n", cadena, strlen(cadena));
    printf("Otra: '%s' sizeof: %ld\n", otra, sizeof(otra));

    /* Esto está MUY MAL: otra = cadena; */
    strcpy(otra, cadena);
    printf("Otra: '%s' strlen: %ld\n", otra, strlen(otra));

    /* Esto está MUY MAL:
    if ( cadena == "Hola" ) {
        printf("cadena es Hola\n");
    } */

    if ( strcmp(cadena, "Hola") == 0 ) {
        printf("cadena es Hola\n");
    }

    strcpy(otra, "Hola");

    /* Esto está MUY MAL:
    otra = otra + "Mundo"; concatenación en Java!?
    */
    strcat(otra, " ");
    strcat(otra, "Mundo");
    printf("Otra: '%s' strlen: %ld\n", otra, strlen(otra));

    strcpy(otra, "");
    printf("Otra: '%s' strlen: %ld\n", otra, strlen(otra));

    return 0;
}


# Funciones

## Punto medio

* Escribir una función que, dados dos números en coma flotante 
calcule y retorne como resultado el punto medio entre ambos.

* Compruebe la función con números positivos, negativos y cero.

## Par y positivo

* Escribir una función que escriba en la salida estándar
si un número entero dado es par (o no) y si es positivo (o no)

* Compruebe la función con distintos (todos los) casos posibles.

## Máximo de 3 números

* Escribir una función que lea de la entrada estándar 3 números
en coma flotante (precondición: son distintos) y retorne 
su máximo.

* Compruebe la función con distintos (todos los) casos posibles.

## Cociente y resto

* Escribir una función que, dados dos números enteros, calcule
y devuelva como resultado el cociente y resto de la división
entera. 

* Comprueba la función

## Intercambiar

* Escribir una función que permita intercambiar el valor de
dos variables. 

* Comprueba la función

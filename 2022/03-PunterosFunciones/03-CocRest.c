
#include<stdio.h>

int cociente_resto(int dividendo, int divisor, int *p_resto) 
{
    int cociente;
    *p_resto = dividendo % divisor;
    cociente = dividendo / divisor;
    printf("Funcion dice: El cociente es %d y el resto es %d\n", cociente, *p_resto);
    return cociente;
}

int main()
{
    int a = 20;
    int c = 0, r = -8;
    c = cociente_resto(a, 3, &r);
    printf("El cociente es %d y el resto es %d\n", c, r);
    return 0;
}

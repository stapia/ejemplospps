# Punteros

## Dos punteros y una variable.

* Navegar al enlace: [https://pythontutor.com/c.html#mode=edit]
* Escribir el siguiente programa

```
int main() {
  int a;
  int *p1, *p2;
  
  a = 10;
  p1 = &a;
  printf("a es %d, p1 es %p\n", a, p1);
  
  *p1 = 33;
  printf("a es %d, p1 es %p\n", a, p1);

  p2 = p1;
  printf("a es %d, p1 es %p p2 es %p\n", a, p1, p2);
  
  *p2 = 99;
  printf("a es %d, pq es %p\n", a, p1);

  return 0;
}
```

Explicar el comportamiento del programa. 

## Dos variables y un puntero.

* Escribir el siguiente programa

```
int main() {
  int a, b;
  int *p;
  
  a = 11;
  b = 99;
  p = &a;
  printf("a = %d, b = %d, p es %p\n", a, b, p);
  
  *p = 33;
  printf("a = %d, b = %d, p es %p\n", a, b, p);

  p = &b;
  printf("a = %d, b = %d, p es %p\n", a, b, p);
  
  *p = 77;
  printf("a = %d, b = %d, p es %p\n", a, b, p);

  return 0;
}
```

Explicar el comportamiento del programa. 

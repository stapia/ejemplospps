
#include<stdio.h>

double medio(double a, double b) {
    return (a + b) / 2;
}

int main()
{
    double m;

    m = medio(-2, +2);
    printf("El medio es: %f\n", m);

    m = medio(-1, +2);
    printf("El medio es: %f\n", m);

    m = medio(-1, -7.2);
    printf("El medio es: %f\n", m);

    return 0;
}

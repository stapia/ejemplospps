
#include <stdio.h>

#define TAM 20

/* array se pasa por referencia */
int leer(double array[TAM]) {
    int i, N;
    scanf("%d", &N);
    for ( i = 0; i < N; i++ ) {
        scanf("%lf", &(array[i]));
    }
    return N;
}

void escribir(double array[], int N) {
    int i;
    printf("#%d: ", N);
    for ( i = 0; i < N; i++ ) {
        printf("%f ", array[i]);
    }
    printf("\n");
}

/* Precondicion N > 0 */
double media(double *array, int N) {
    int i;
    double suma = 0;
    for ( i = 0; i < N; i++ ) {
        suma += array[i];
    }
    return suma / N;
}

int main()
{
    int tam; double datos[TAM]; double media_datos;
    tam = leer(datos);
    escribir(datos, tam);
    media_datos = media(datos, tam);
    printf("la media es %f\n", media_datos);
    return 0;
}


# Arrays dimensión 1

## Calcular el número de pares desordenados

* Escribir un archivo de texto plano con un número, `N`, que 
indica el tamaño de un array seguido de N número en coma flotante. 

* Escriba un programa que lea de la entrada
estándar los datos de un array tal y como se
han escrito en el archivo anterior.

* Precondición: 2 <= N <= 20.

* Y cuente el número de pares desordenados. Es decir, 
que cumplan que Vi >= Vi+1.

## Pedir, mostrar y calcular la media de arrays

* Escribir una función que lea los datos de un array
tal y como se han indicado en el problema anterior.

* Escribir una función que escriba el contenido de
un array, empezando por el número de componentes 
que tiene.

* Escribir una función que calcule la media aritmética
de un array y la retorne como resultado.

* Escribir el programa que comprueba las funciones
anteriores.

## Separando funciones

* Copie las funciones anteriores en un archivo aparte.

* Escriba las declaraciones en un archivo de cabeceras.

* Escriba un programa que use las funciones anteriores.

## Recortar arrays

Escriba un programa que use las funciones anteriores
de manera que:

* Lea los datos del array de la entrada estándar.

* Escriba en la salida estándar las componentes entre
la 3ª y la 5ª ambas inclusive. Es decir, hay que saltarse
2 posiciones (1ª y 2ª) y escribir 3 (3ª, 4ª y 5ª).

* Calcule y escriba en la salida la media de las componentes
del vector entre la 3ª y la 5ª ambas inclusive. 


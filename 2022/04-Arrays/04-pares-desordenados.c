
#include<stdio.h>

#define TAM 20

int main() {
    int N, i;
    /*double x;*/
    double array[TAM];
    int pares = 0;

    scanf("%d", &N);

    for ( i = 0; i < N; i++ ) {
        /* scanf("%lf", &x);
        array[i] = x;
        printf("x = %f\n", x);*/
        scanf("%lf", &(array[i]));
    }

    for ( i= 0; i < N -1; ++i ) {
        if ( array[i] >= array[i+1] ) {
            pares++;
        }
    }

    printf("Pares desordenados: %d\n", pares);

    return 0;
}
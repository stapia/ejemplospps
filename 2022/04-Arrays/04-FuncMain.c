
#include <stdio.h>
#include "04-FuncArray.h"

#define TAM 20

int main()
{
    int tam; double datos[TAM]; double media_datos;
    tam = leer(datos);
    printf("tam es %d\n", tam);
    /* Salta 2 posiciones (empieza en la tercera, cuyo índice es el 2) 
    y termina cuando lleve 3 elementos */
    escribir(datos+2, 3);
    media_datos = media(datos+2, 3);
    printf("la media es %f\n", media_datos);
    return 0;
}


#include<stdio.h>

int main()
{
    // Cuidado este comentario no es ANSI
    /* code */

    unsigned int entero_sin_signo ; /* Esto es declarar */
    unsigned short corto = -1, otro = -2; /* esto es inicializar */
    /* Es lo mismo que unsigned short int */
    int suma;
    double division;

    entero_sin_signo = -1; /* Esto es asignar */

    printf("Entero es %d\n", entero_sin_signo);
    printf("Entero sin signo es %u\n", entero_sin_signo);

    printf("corto es %d\n", corto);

    suma = corto + otro;
    printf("suma es %d\n", suma);

    printf("5 / 11 es %d\n", 5 / 11);
    
    division = 5 / 11;
    printf("5 / 11 es %f\n", division);

    return 0;
}

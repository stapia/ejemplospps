
#include<stdio.h>

int main()
{
    char caracter, mayuscula;
    int bool1;

    scanf("%c", &caracter);

    bool1 = 'a' <= caracter && caracter <= 'z' ||
            'A' <= caracter && caracter <= 'Z';

    printf("El caracter es una letra es: %d", bool1);
    printf("El caracter es una letra es: %s", bool1 ? "Cierto" : "Falso");

    mayuscula = 'a' <= caracter && caracter <= 'z' ? 
                caracter -'a' + 'A' : 
                caracter;
    
    printf("En mayuscula: %c", mayuscula);
    return 0;
}

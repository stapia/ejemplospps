# Expresiones

## Escribir un programa que:

* Declare una variable como un entero sin signo.
* Le asigne el valor -1.
* Muestre su valor por pantalla. 
* Haga lo mismo con un unsigned short. 
* Calcule la suma de dos variables unsigned short con valores -1 y -2
y guarde el resultado en un int. Muestre el resultado.
* Muestre el resultado de 5 / 11.

## Escribir otro programa que:

* Calcule el cuadrado y el cubo de un número entero

* Ídem de un número en coma flotante

Los números deben leerse del canal de entrada estándar y 
escribirse los resultados en el canal de salida.

## Escribir otro programa que:

* Calcule el cociente de la división entera de dos
números (enteros).

* Calcule el resto de la división entera de dos
números (enteros).

* Los números debe leerse suponiendo que, en el momento
de teclear sus valores, se separan con una coma. 

## Escribir otro programa que:

* Muestre si un carácter introducido por teclado es una letra
(de la 'a' a la 'z' ó en mayúsculas).

* Muestre si la misma letra es un dígito (numérico, del '0' al '9').

* Muestre la misma letra en mayúscula (si es una letra minúscula)
o la deje tal cuál en caso contrario.

* ¿Cómo se puede hacer para que nunca lea un *blanco* como letra?

## Escribir otro programa que:

* Lea del canal de entrada un número entero de hasta 3 dígitos
y obtenga los bits 1, 2, 3 y 4 de su representación binaria y
muestre el propio número y el valor de los bits en el canal
de salida.

* **Sin** usar operaciones aritmeticas.

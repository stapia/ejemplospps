
#include<stdio.h>
#include<math.h>

int main()
{
    int entero, cubo;
    double x, cubo_x;

    scanf("%d %lf", &entero, &x);

    cubo = entero * entero * entero;
    cubo_x = pow(x, 3);

    printf("Cubo: %d\tCubo (double): %.14f\n", cubo, cubo_x);

    return 0;
}

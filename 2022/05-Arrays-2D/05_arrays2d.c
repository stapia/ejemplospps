#include <stdio.h>

void escribeSize(int array[])
{
    printf("(escribeSize) size array %lu\n", sizeof(array));
}

int main()
{
    int arrayEnteros[] = {1, 2, 3, 4, 5};

    int array2d[][3] = {
        {11, 2, 3},
        {110, 220},
        {440, 550, 660},
        {770, 880, 990}
    };

    printf("%d %d\n", arrayEnteros[0], array2d[1][2]);
    printf("El (1,5) es %d\n", array2d[1][5]);

    printf("sizeof(arrayEnteros) = %ld\n", sizeof(arrayEnteros));
    printf("sizeof(array2d) = %ld\n", sizeof(array2d));

    escribeSize(arrayEnteros);

    return 0;
}

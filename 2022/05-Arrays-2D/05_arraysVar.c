
#include <stdio.h>

#define TAM 20

/* Precondicion: hay mucho 20 filas y 20 columnas */
void leer(double matriz[][TAM], int *p_nFilas, int *p_nCols)
{
    int i, j;
    char letra;

    scanf("%d %d", p_nFilas, p_nCols);

    for (i = 0; i < *p_nFilas; ++i)
    {
        double x;
        j = 0;
        while (scanf("%lf", &x) == 1)  {
            matriz[i][j] = x;
            ++j;
        }
        for (; j < *p_nCols; j++)
        {
            matriz[i][j] = 0.0;
        }
        scanf(" %c", &letra);
    }
}

/* Precondicion: hay como mucho 20 filas y 20 columnas */
void escribir(double (*matriz)[TAM], int nFilas, int nCols) {
    int i, j;
    for (i = 0; i < nFilas; ++i)
    {
        for (j = 0; j < nCols; ++j)
        {
            printf("%7.2f ", matriz[i][j]);
        }
        printf("\n");
    }
}

int main()
{
    double mat[TAM][TAM];
    int nFilas, nCols;
    leer(mat, &nFilas, &nCols);
    escribir(mat, nFilas, nCols);
    printf("Con mat+1 y nFilas 3:\n");
    escribir(mat+1, 3, nCols);
    return 0;
}

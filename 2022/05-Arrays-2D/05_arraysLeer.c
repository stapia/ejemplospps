
#include <stdio.h>

double matriz[5][4]; /* Mala practica */

int main()
{
    int i, j; char letra;

    for (i = 0; i < 5; ++i)
    {
        double x;
        j = 0;
        while ( scanf("%lf", &x) == 1 ) 
        {
            matriz[i][j] = x;
            ++j;
        }
        for ( ; j < 4; j++ ) {
            matriz[i][j] = 0.0;
        }
        scanf(" %c", &letra);
    }

    for (i = 0; i < 5; ++i)
    {
        for (j = 0; j < 4; ++j) {
            printf("%7.2f ", matriz[i][j]);
        }
        printf("\n");
    }

    return 0;
}


#include <stdio.h>

/* Precondicion: hay 5 filas */
void leer(double matriz[][4])
{
    int i, j;
    char letra;

    for (i = 0; i < 5; ++i)
    {
        double x;
        j = 0;
        while (scanf("%lf", &x) == 1)  {
            matriz[i][j] = x;
            ++j;
        }
        for (; j < 4; j++)
        {
            matriz[i][j] = 0.0;
        }
        scanf(" %c", &letra);
    }
}

/* Precondicion: hay 5 filas */
void escribir(double matriz[][4]) {
    int i, j;
    for (i = 0; i < 5; ++i)
    {
        for (j = 0; j < 4; ++j)
        {
            printf("%7.2f ", matriz[i][j]);
        }
        printf("\n");
    }
}

int main()
{
    double mat[5][4];
    leer(mat);
    escribir(mat);
    return 0;
}

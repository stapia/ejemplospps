
# Arrays de 2 ó más dimensiones

## Declaración con inicialización y tamaños

**Escribir un programa que:**

* Declare e inicialice un array con valores enteros: 

```
{ 1, 2, 3, 4, 5 }
```

* Declare e inicialice un array 2D con valores enteros:

```
{ 
  {   1,    2,    3 },
  { 110,  220,  330 },
  { 440,  550,  660 },
  { 770,  880,  990 }
}
```

*Nota: El momento de la inicialización es el único en que se puede
dar valores a un array (1D, 2D,...) de esta manera.*

* Quite el tamaño del array 1D y el tamaño asociado al número de
filas del array 2D. ¿Es posible? ¿Por qué? ¿Se puede quitar
el número columnas del array 3D? ¿Por qué? Prueba a quitar 
una componente en cualquier fila. 

* Escriba las primeras coordenadas de ambos (para comprobar). 

* Escriba la componente (1, 5) del array 2D, razone el resultado. 

* Sabiendo que el tamaño de una variable (o tipo de dato) se
puede obtener mediante el operador sizeof, calcule y escriba 
en la salida el tamaño de ambos arrays. 

* Escriba la siguiente función: 

```
void escribeSize(int array[])
{
    printf("(escribeSize) size array %lu\n", sizeof(array));
}
```

* Llame a la función (se debe ignorar el aviso si sale) ¿Por qué se
muestra ese valor? ¿De dónde viene?

## Arrays 2D 

**Escribir un programa que:**

* Lea un array 2D de la entrada estándar teniendo en cuenta que:

1. El array tiene 5x4 números en coma flotante (5 filas, 4 columnas).
1. Cada fila del array se puede leer en la entrada hasta encontrar 
un signo de punto y coma. 
1. Si los números leídos son menos de 4 el resto de la fila se 
completa con 0. 
1. Precondición: No hay más de 4 números en cada fila de la entrada.
1. Tomese como ejemplo de formato el archivo *dato1.txt*

* Escriba el array en el canal de salida estándar separando cada
número del siguiente con un espacio y terminando cada fila en un
salto de línea. 

## Arrays 2D y funciones

**Usando como punto de partida el programa anterior:**

* Mueva la lectura de los datos del array a una función y:

1. Rectifique la forma de leer para que se lea en primer
lugar las dimensiones de la matriz (número de filas y 
luego número de columnas).
1. Produzca los resultados necesarios.
1. Precondición: ``0 < nFilas, nColumnas <= TAM (20)``
1. El ejemplo del formato es el archivo *datos2.txt*

* Mueva la escritura de los datos del array a una función

* Escriba main para que compruebe ambas funciones. 

* Declare un array con un número de columnas distinto e 
intente pasarlo como argumento de la función de escritura.
¿Qué pasa? ¿Cuál es el error?

* Cambie la declaración de una de las funciones a la alternativa
de declaración de parámetros tipo array 2D basada en punteros. 
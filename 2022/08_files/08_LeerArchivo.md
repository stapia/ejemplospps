
# Leer archivos

## Leer palabras

Escribir un programa que:

* Abra el archivo de texto "palabras.txt" para leer.
* Lea palabras (máximo 31 caracteres) del archivo.
* Concatene todas ellas separandolas con un carácter ';'

## Leer puntos 2D y 3D

Escribir un programa que:

* Reciba como parámetro el nombre de un archivo (utilizaremos puntos.txt)
* Lea cada línea del archivo sabiendo que contiene un punto en 2d ó 3d.
* Guarde cada punto 2D en un array (dinámico). 
* Guarde cada punto 3D en una lista (en orden inverso)
* Escribir los punto 2d y luego los 3d.

*Sobre la solución:*

### Usar typedef

typedef sirve para declarar *alias* de tipos de datos. 

Se usa typedef para simplificar la escritura de tipos. Por ejemplo, en 
la función procesar hay que pasar el puntero a los puntos 2d por *referencia*
(hay que cambiarlo dentro de la función), la declaración sería bastante 
confusa, sin embargo, con el typedef se debería leer un poco mejor.

En este ejemplo, el tipo `pPunto2d` es un *puntero* que apunta a
dos `double`. Servía para tener un array dinámico de puntos en 2d.

Como ejercicio se puede usar para definir primero los Puntos2d
y luego el puntero a esos puntos. 

### Sobre la declaración de la función procesar

Esta función necesita proporcionar 3 resultados: el puntero al primer
nodo de la lista, el puntero al array dinámico de puntos 2d y el 
número de puntos 2d leídos. 

Se ha optado por poner el primer nodo de la lista como retorno y los
otros dos como parámetros **por referencia**. 

Se puede comprobar la diferencia respecto de las funciones que 
muestran el contenido de los datos correspondientes.

### Sobre memcpy

En `procesar` se usa `memcpy` para copiar el punto 3d en el nodo de 
la lista. Básicamente esta función sirve para copiar *bloques* de 
memoria. Suelen utilizarse con frecuencia para copiar arrays 
completos. 

### Sobre realloc

En `procesar` se usa `realloc` para incrementar el tamaño del array
de puntos 2d en un solo paso. Para usar esta función se debe tener
en cuenta que ya se debe hacer usado alloc (o similares) para 
crear el array dinámico y que el resultado debe ser liberado 
con free. Como ventaja, realloc se encarga de copiar lo que 
ya teníamos guardado y de hacer el free del array *antiguo*.

### Sobre fgets

Cuando hay que leer un archivo por líneas (es decir, cuando el final de línea es un
separador importante en el contenido del archivo), es muy útil la función `fgets`.

Esta función lee una línea (hasta el tamaño indicado) del archivo dado y la 
guarda en el *buffer* dado. A tener en cuenta:

* Retorna la dirección del mismo *buffer* o NULL si no se puede leer.
* Deja el salto de línea en el *buffer*.
* Nunca desborda el *buffer*, si la línea es más larga deja de leer. 

Una vez procesada la línea y almacenada en un *string* se procesa esa cadena. 
Alternativas: sscanf, strtok, de caracter en caracter... Dependiendo del formato
de cada línea. 





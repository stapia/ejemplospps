
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef double (*pPunto2d)[2];

struct nodo
{
    double v[3];
    struct nodo* sig;
};

typedef struct nodo nodo_t;

/* Precondicion: longitud lineas < 256 */
nodo_t* procesar(FILE* g, pPunto2d* arrayP2d, size_t *numP2d) {
    char linea[256]; 
    int line_count = 0, leidos = 0;
    size_t N = 4;
    double aux_V[3];
    nodo_t *prim = NULL;
    *arrayP2d = malloc(N*sizeof(double[2]));
    *numP2d = 0;
    while ( fgets(linea, 256, g) ) {
        ++line_count;
        leidos = sscanf(linea, "%lf , %lf , %lf", aux_V, aux_V+1, aux_V+2);
        if ( leidos == 2 ) {
            if ( *numP2d + 1 >= N ) {
               N *= 2;
               *arrayP2d = realloc(*arrayP2d, N * sizeof(double[2]));
            }
            (*arrayP2d)[*numP2d][0] = aux_V[0];
            (*arrayP2d)[*numP2d][1] = aux_V[1];
            ++(*numP2d);
        } else if ( leidos == 3 ) {
            nodo_t *aux = malloc(sizeof(nodo_t));
            memcpy(aux->v, aux_V, sizeof(double[3]));
            aux->sig = prim;
            prim = aux;
        } else {
            fprintf(stderr, "Linea #%d: No se ha podido leer el punto\n", line_count);
        }
    }
    return prim;
}

/* void mostrar2d(pPunto2d arrayP2d, size_t numP2d) */
void mostrar2d(double arrayP2d[][2], size_t numP2d) {
    size_t i;
    for ( i = 0; i < numP2d; ++i ) {
        printf("P2d: %5.2f %5.2f\n", arrayP2d[i][0], arrayP2d[i][1]);
    }
}

void mostrar3d(nodo_t *prim)
{
    nodo_t *aux = prim;
    while ( aux != NULL ) {
        printf("P3d: %5.2f %5.2f %5.2f\n", aux->v[0], aux->v[1], aux->v[2]);
        aux = aux->sig;
    }
}

void liberar(nodo_t *prim) {
    nodo_t *aux;
    while (prim != NULL) {
        aux = prim;
        prim = prim->sig;
        free(aux);
    } 
}

int main(int argc, char const *argv[])
{
    FILE *g;
    if ( argc != 2 ) {
        fprintf(stderr, "usage: %s <filename>\n", argv[0]);
        return 1;
    }
    if ( (g = fopen(argv[1], "r")) != NULL ) {
        pPunto2d arrayP2d;
        size_t numP2d;
        nodo_t *prim;
        prim = procesar(g, &arrayP2d, &numP2d);
        mostrar3d(prim);
        mostrar2d(arrayP2d, numP2d);
        liberar(prim);
        free(arrayP2d);
        fclose(g);
    } else {
        fprintf(stderr, "can not open file `%s'\n", argv[1]);
        return 2;
    }
    return 0;
}

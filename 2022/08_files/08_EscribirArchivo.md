
# Escribir archivos

## Escribir en el canal de errores

Escribir un programa que:

* Tome dos argumentos, los sume y escriba el resultado en
la salida estándar.

* Si alguno de los argumentos no es un número (coma flotante)
escriba un mensaje de error en el canal estándar de errores.

## Escribir números aleatorios y su varianza

Escribir un programa que:

* Tome un número entero como argumento,

* Genere un array de ese tamaño con números aleatorios,

* [ Calcule la varianza (y se escriba en la salida estándar), ]

* Guarde los números en un archivo de texto de nombre "datos.out"

Si se encuentra un error se debe escribir un mensaje en el canal de errores. 
Por ejemplo:

- No se consigue abrir el archivo.
- El argumento no tenga un valor válido.
- No se pueda *generar* el vector.
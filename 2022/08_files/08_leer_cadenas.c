
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN_MAX 32

void procesar(FILE* g) {
    char palabra[STR_LEN_MAX];
    while ( fscanf(g, "%31s", palabra) == 1 ) {
        printf("palabra: '%s'\n", palabra);
    }
}

/* Leer palabras del archivo concatenado todas ellas
y, a la vez, separandolas por un punto y coma. La 
concatenacion se guarda en una cadena dinámica */
char* procesar_concat(FILE* g) {
    char palabra[STR_LEN_MAX];
    int N = STR_LEN_MAX;
    char *resultado = malloc(N*sizeof(char));
    int lenP, lenR;
    strcpy(resultado, "");
    /* resultado[0] = '\0'; */
    while ( fscanf(g, "%31s", palabra) == 1 ) {
        lenR = strlen(resultado);
        lenP = strlen(palabra);
        if ( lenP + lenR + 1 /* ; */ + 1 /* '\0' */> N ) {
            /* Hace resultado más grande */
            char *aux = malloc(2*N*sizeof(char));
            strcpy(aux, resultado);
            free(resultado);
            resultado = aux;
            N = 2 * N;
            /* resultado = realloc(resultado, 2*N*sizeof(char)); */
        }
        strcat(resultado, palabra);
        strcat(resultado, ";");
        printf("palabra: '%s'\n", palabra);
    }
    return resultado;
}

int main()
{
    FILE *g; char *resultado;

    g = fopen("palabras.txt", "r");
    if ( g != NULL) {
        resultado = procesar_concat(g);
        printf("%s\n", resultado);
        free(resultado);
        fclose(g);
    } else {
        fprintf(stderr, "No se puede abrir\n");
    }

    return 0;
}

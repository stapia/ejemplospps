
#include <stdio.h>

#define DIM 80

int main() {
	char cadena[DIM];
	char *a_punteros[5];
	int encontrados = 0;
	char *aux;
	int i; 
	
	fgets(cadena, DIM, stdin);
	
	printf("cadena: `%s`\n", cadena);
	
	for ( aux = cadena; *aux; ++aux) {
		if ( *aux == ' ' ) {
			a_punteros[encontrados] = aux + 1;
			++encontrados;
		}
	}
	
	for ( i = 0; i < encontrados; ++i ) {
		printf("a_punteros[i]: `%s`\n", a_punteros[i]);
	}
	
	return 0;
}

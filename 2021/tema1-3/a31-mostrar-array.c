
#include <stdio.h>

void escribir(unsigned short *aux, int n_elem) {
	int i, tam, size_short;
	
	tam = sizeof(aux);
	size_short = sizeof(short);
	// n_elem = tam / size_short;
	
	printf("tam = %d, size_short = %d, n_elem = %d\n", 
	       tam, size_short, n_elem);
	       
	for ( i = 0; i < n_elem; ++i ) {
		printf("%d ", aux[i]);
	}
	
	printf("\n");
}

int main() {
	unsigned short array[] = { -1, 5, 7, 20, 12 };
	
	escribir(array, sizeof(array) / sizeof(short));
	
	return 0;
}


#include <stdio.h>

void ordenando(int* pa, int *pb) {
	if ( *pb > *pa ) {
		int aux = *pb;
		*pb = *pa; 
		*pa = aux;
	}
	printf("En la función: *pa = %d, *pb = %d\n", *pa, *pb);
}


void ordenando3(int *pa, int* pb, int* pc) {
	ordenando(pa, pb);
	ordenando(pa, pc);
	ordenando(pb, pc);
}


int main() {
	
	int a = 20, b = 44, c = 33;
	
	ordenando3(&a, &b, &c);
	printf("En main: a = %d, b = %d, c = %d\n", a, b, c);

	return 0;
}

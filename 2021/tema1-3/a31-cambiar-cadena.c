
#include <stdio.h>

#define DIM 80

void escribir(char *aux) {
	int i;
	for ( i = 0; *aux != '\0'; ++i, aux = aux + 1) {
		printf("Letra %d: %c\n", i, *aux);
	}	
}

int main() {
	char cadena[DIM];
	//char *aux; 
	int i;
	
	scanf("%79s", cadena);
	
	// for ( i = 0; cadena[i] != '\0'; ++i ) {
	for ( i = 0; cadena[i] != 0; ++i ) {
		if ( 'a' <= cadena[i] && cadena[i] < 'z' ) {
			cadena[i] = cadena[i] + 1;
		}
	}
	
	printf("Cadena: `%s`\n", cadena);
	
	// aux = cadena;
	escribir(cadena);
	
	printf("A partir de la tercera letra:\n");
	
	escribir(cadena+2);

	printf("Cadena: `%s`\n", cadena);
	
	return 0;
}

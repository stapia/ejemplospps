
#include <stdio.h>

void ordenandoMal(int a, int b) {
	if ( b > a ) {
		int aux = b;
		b = a; 
		a = aux;
	}
	printf("En la función: a = %d, b = %d\n", a, b);
}

int main() {
	
	int a = 20, c = 33;
	
	ordenandoMal(a, c + 3);
	printf("En main: a = %d, c = %d\n", a, c);

	return 0;
}

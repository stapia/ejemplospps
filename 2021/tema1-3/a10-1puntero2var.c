
#include <stdio.h>
int main() {
	int numero = 17;
	int otra = 88;
	int *q;
	
	q = &numero;
	printf("numero = %d, puntero = %p\n", numero, q);
	
	*q = 22;
	printf("numero = %d, puntero = %p\n", numero, q);
	
	printf("otra = %d, puntero = %p\n", otra, q);
	q = &otra;
	*q = 33;
	printf("numero = %d, otra = %d, puntero = %p\n", numero, otra, q);

	otra = 44;
	printf("otra = %d, puntero = %p\n", *q, q);

	return 0;
}


#include <stdio.h>

void ordenando(int* pa, int *pb) {
	if ( *pb > *pa ) {
		int aux = *pb;
		*pb = *pa; 
		*pa = aux;
	}
	printf("En la función: *pa = %d, *pb = %d\n", *pa, *pb);
}

int main() {
	
	int a = 20, c = 33;
	
	ordenando(&a, &c);
	printf("En main: a = %d, c = %d\n", a, c);

	return 0;
}

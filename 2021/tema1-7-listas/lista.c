
#include <stdio.h>
#include <stdlib.h>

struct nodo {
  int dato;
  struct nodo* sig;
};

typedef struct nodo nodo_t;

struct lista {
  nodo_t *primero;
  size_t tam;
};

typedef struct lista lista_t;

void print(const lista_t* li, FILE *g) {
  nodo_t *aux = li->primero;
  while ( aux != NULL ) {
    fprintf(g, "%d ", aux->dato);
    aux = aux->sig;
  }
  fprintf(g, "\n");
}

void insertar(lista_t* li, int x) {
  li->tam += 1;
  if ( li->primero == NULL || x < li->primero->dato ) {
    /* Caso A */
    nodo_t *nn = malloc(sizeof(nodo_t)); /*i*/
    nn->dato = x; /* ii*/
    nn->sig = li->primero; /* iii */
    li->primero = nn; /* iv */
  } else {
    /* Caso B */
    nodo_t *anterior = li->primero;
    while ( anterior->sig != NULL && anterior->sig->dato < x ) {
      anterior = anterior->sig;
    }
    
    nodo_t *nn = malloc(sizeof(nodo_t)); /*i*/
    nn->dato = x; /* ii*/
    nn->sig = anterior->sig; /* iii */
    anterior->sig = nn; /* iv */
  }
}

int main() {
  
  int array[] = { 23, -4, 46, 20 };
  int len = sizeof(array) / sizeof(int);
  int i;
  
  lista_t lista_1;
  
  lista_1.primero = NULL;
  lista_1.tam = 0;
  
  for ( i = 0; i < len; ++i ) {
    insertar(&lista_1, array[i]);
  }

  print(&lista_1, stdout);
  return 0;
}

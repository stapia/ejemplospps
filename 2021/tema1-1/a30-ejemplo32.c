
#include <stdio.h>
#include <math.h>

int main() {

  double a = 3.0, b = 4.0, medio;

  while ( fabs(b - a) > 1e-6 ) {
    /* Invariante es que hay un cero entre a y b */
    medio = (a + b) / 2;
    if ( sin(medio) * sin(a) > 0 ) {
      a = medio;
    } else {
      b = medio;
    }
  }

  printf("Pi es %.14f\n", medio);
  return 0;
}


#include <stdio.h>

int main() {
	int numero, resultado;
	scanf("%d", &numero);
	printf("Tu número es: %d\n", numero);
	printf("Tu número es: %x (en hexadecimal)\n", numero);
	resultado = numero * 2;
	printf("y el doble es: %d\n", resultado);
    return 0;
}

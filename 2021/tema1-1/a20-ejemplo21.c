
#include <stdio.h>

int main() {
  char aspecto; int decimas;

  scanf("%c %d", &aspecto, &decimas);

  /* Precondicion: Suponemos que el usuario
   * introduce v, r ó a */

  if ( aspecto == 'v' ) {
    printf("Verde\n");
  } else if ( aspecto == 'r') {
    printf("Rojo\n");
  } else if ( decimas % 2 == 1 ) {
    printf("Amarillo\n");
  } else {
    printf("Ninguna\n");
  }

  return 0;
}

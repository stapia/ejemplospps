
#include <stdio.h>

int main() {

  int i; double fb, fa, df;

  scanf("%lf", &fa); /* 1 dato */

  for ( i = 0; i < 4; ++i ) { /* Se repite 4 más el de antes -> 5 */
    /* Invariante -> fa es el valor anterior */
    scanf("%lf", &fb);
    df = fb - fa;
    printf("%f ", df);
    fa = fb; /* Aquí consigo que vuelva a ser el "anterior" */
  }
  printf("\n");
  return 0;
}

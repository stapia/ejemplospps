
#include <stdio.h>

int main() {

  double fb, fa, df;

  scanf("%lf", &fa); /* 1 dato */

  while ( scanf("%lf", &fb) == 1 ) {
    df = fb - fa;
    printf("%f ", df);
    fa = fb; /* Aquí consigo que vuelva a ser el "anterior" */
  }
  printf("\n");
  return 0;
}

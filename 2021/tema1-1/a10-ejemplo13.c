
#include <stdio.h>
#include <math.h>

int main() {
	double x, raiz;
	scanf("%lf", &x);
	raiz = sqrt(x);
	printf("El número es: %.20f\n", x);
	printf("Y la raiz es: %.10f\n", raiz);
	return 0;
}

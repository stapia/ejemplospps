
#include <stdio.h>
#include <string.h>

int main() {
	char filename[] = "datos.txt";
	FILE *g;
	
	fprintf(stderr, "Abriendo `%s`\n", filename);
	
	g = fopen(filename, "r"); /* r -> read */
	
	if ( g != NULL ) {
		char linea[128];
		int i = 1;
		while ( fgets(linea, 128, g) != NULL ) {
			printf("%d [%ld]: %s", i, strlen(linea), linea);			
			++i;
		}
		printf("fin\n");
		fclose(g);
	} else {
		fprintf(stderr, "No se pudo abrir `%s`\n", filename);
	}
	
	return 0;
}


#include <stdio.h>
#include <string.h>

int main() {
	char filename[] = "datos.txt";
	FILE *g;
	
	fprintf(stderr, "Abriendo `%s`\n", filename);
	
	g = fopen(filename, "r"); /* r -> read */
	
	if ( g != NULL ) {
		char caracter;
		while ( fscanf(g, "%c", &caracter) == 1 ) {
			if ( caracter <= ' ' ) { /* caracter <= 32 */
				printf("%d ", caracter);
			} else {
				printf("%c ", caracter);
			}
		}
		printf("fin\n");
		fclose(g);
	} else {
		fprintf(stderr, "No se pudo abrir `%s`\n", filename);
	}
	
	return 0;
}

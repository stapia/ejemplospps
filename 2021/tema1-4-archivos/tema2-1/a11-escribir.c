
#include <stdio.h>

int main(int argc, char *argv[]) {
	FILE *g;
	
	if ( argc == 2 ) {

		fprintf(stderr, "Abriendo `%s`\n", argv[1]);
		
		g = fopen(argv[1], "w"); /* w -> write */
		
		if ( g != NULL ) {
			fprintf(g, "hola mundo\n");
			fprintf(g, "%d\t%d", 3424, -23);
			fclose(g);
		} else {
			fprintf(stderr, "No se pudo abrir `%s`\n", argv[1]);
		}
	} else {
		return 1;
	}
	
	return 0;
}

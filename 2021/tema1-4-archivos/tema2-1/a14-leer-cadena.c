
#include <stdio.h>
#include <string.h>

int main() {
	char filename[] = "datos.txt";
	FILE *g;
	
	fprintf(stderr, "Abriendo `%s`\n", filename);
	
	g = fopen(filename, "r"); /* r -> read */
	
	if ( g != NULL ) {
		char cadenaA[64], cadenaB[64];
		while ( fscanf(g, "%63s %63s", cadenaA, cadenaB) == 2 ) {
			printf("Cadenas leídas: A$%s$ B$%s$\n", cadenaA, cadenaB );
		}
		printf("fin\n");
		fclose(g);
	} else {
		fprintf(stderr, "No se pudo abrir `%s`\n", filename);
	}
	
	return 0;
}

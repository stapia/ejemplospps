
#include <stdio.h>
#include <string.h>

int main() {
	char filename[] = "lineas.txt";
	FILE *g;
	
	fprintf(stderr, "Abriendo `%s`\n", filename);
	
	g = fopen(filename, "r"); /* r -> read */
	
	if ( g != NULL ) {
		char linea[128];
		char cad1[21], cad2[21], *aux;
		double numero; int entero, otro;
		int i = 1;
		while ( fgets(linea, 128, g) != NULL ) {
			fprintf(stderr, "%d [%ld]: %s", i, strlen(linea), linea);
			
			switch ( linea[0] ) {
				case '1': 
					sscanf(linea + 2, "%20s %20s", cad1, cad2);
					printf("cad1: %s cad2: %s\n", cad1, cad2);
				break;
				case '2': 
					sscanf(linea + 2, "%lf / %i", &numero, &entero);
					printf("num: %f entero: %d\n", numero, entero);
				break;
				case '3': 
					sscanf(linea + 2, "%12s / %3d%3d", cad1, &entero, &otro);
					printf("cad1: %s entero: %d otro: %d\n", cad1, entero, otro);
				break;
				case '4': 
					/* %[] lee mientras que las letras leídas estén entre los corchetes */
					/* %[^] lee mientras que las letras leídas NO estén entre los corchetes */
					sscanf(linea + 2, "%12[a-z]%d", cad1, &entero);
					printf("cad1: %s entero: %d\n", cad1, entero);
				break;
				case '5': 
					aux = strtok( linea + 2, " ,.;\n");
					while ( aux != NULL ) {
						printf("%s/", aux); /* Separo con "/" */
						/* siguiente "token" */
						/* Atención en las sucesivas llamadas hay que usar NULL */
						/* La función guarda internamente por donde va */
						aux = strtok(NULL, " ,.;\n");
					}
					printf("\n");
				break;
				
				default:
					fprintf(stderr, "Formato no válido en %d\n", i);
				break; 
			}
			
			++i;
		}
		printf("fin\n");
		fclose(g);
	} else {
		fprintf(stderr, "No se pudo abrir `%s`\n", filename);
	}
	
	return 0;
}

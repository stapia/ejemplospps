
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct datos {
  int numero;
  char cadena[20];
  char *puntero_cad;
};

typedef struct datos datos_t;

int main() {
  
  datos_t data1 = { 5, "hola", NULL }; 
  datos_t data2;
  datos_t *p_data;
  
  printf("data1.numero = %d\n", data1.numero);
  
  p_data = &data1;
  printf("p_data->numero = %d\n", p_data->numero);
  
  data2.numero = -8;
  strcpy(data2.cadena,"adios");
  data2.puntero_cad = malloc(10);
  strcpy(data2.puntero_cad,"uno");
  
  /* data1.puntero_cad = data1.cadena + 1; */
  
  data1 = data2;

  return 0;
}

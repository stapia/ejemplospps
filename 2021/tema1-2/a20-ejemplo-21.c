
#include <stdio.h>

#define DIM 64

int main() {
	double a[DIM], b[DIM], result[DIM];
	int size_a = 0, size_b = 0;
	double aux; 
	char letra;
	
	while ( scanf("%lf", &aux) == 1 ) {
		a[size_a] = aux;
		size_a = size_a + 1;
	}
	
	scanf(" %c", &letra);

	while ( scanf("%lf", &aux) == 1 ) {
		b[size_b] = aux;
		++size_b;
	}
	
	if ( size_a == size_b ) {
		int i; 
		for ( i = 0; i < size_a; ++i ) {
			result[i] = a[i] + b[i];
			printf("%f ", result[i]);
		}
		printf("\n");
	} else {
		return 1;
	}
	
	return 0;
}

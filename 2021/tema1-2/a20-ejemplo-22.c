
#include <stdio.h>

#define DIM 80

int main() {
	char linea[DIM];
	
	while ( fgets(linea, DIM, stdin) != NULL ) {
		/* printf("$$$ %s $$$\n", linea); */
		int contador = 0; 
		int i;
		for ( i = 0; linea[i] != '\0' ; ++i ) {
			if ( '0' <= linea[i] && linea[i] <= '9') { /* Números que hay */
				++contador; 
			}
		}
		printf("Números en esta línea: %d\n", contador);
	}
	
	return 0;
}


#include <stddef.h> /* Para size_t */

double calcular_varianza(const double *array, size_t tam, double media) {
  double varianza = 0;
  size_t i;
  for ( i = 0; i < tam; ++i ) {
    varianza += (array[i]-media)*(array[i]-media);
  }
  varianza /= tam;
  return varianza;
}

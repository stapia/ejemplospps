
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char** argv) {

  FILE *g; int tam, i; double dato;
  double * array;
  double media, varianza;

  if ( argc == 2 ) {
    g = fopen(argv[1], "r");
  } else {
    g = stdin;
  }

  fscanf(g, "%d", &tam);
  array = malloc(tam*sizeof(double));

  for ( i = 0; i < tam; ++i ) {
    fscanf(g, "%lf", &dato);
    array[i] = dato; /* A[b] <=> *(A + (b)) */
  }

  media = 0;
  for ( i = 0; i < tam; ++i ) {
    media += array[i];
  }
  media /= tam;

  varianza = 0;
  for ( i = 0; i < tam; ++i ) {
    varianza += (array[i]-media)*(array[i]-media);
  }
  varianza /= tam;

  printf("Media: %f, Varianza = %f\n", media, varianza);

  /* Libero variable dinámica */
  free(array);

  /* Cierro el archivo */
  if ( argc == 2 ) {
    fclose(g);
  }

  return 0;
}

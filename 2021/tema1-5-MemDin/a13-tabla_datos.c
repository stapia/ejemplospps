
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h> /* Para size_t */

double calcular_media(const double *, size_t );

#include "a13_varianza.h"

double* leer(FILE* g, int* p_tam) {
  double *array; double dato;
  int i;
  fscanf(g, "%d", p_tam);
  array = malloc((*p_tam)*sizeof(double));
  for ( i = 0; i < (*p_tam); ++i ) {
    fscanf(g, "%lf", &dato);
    array[i] = dato; /*  A[b] <=> *(A + (b)) */
  }
  return array;
}


int main(int argc, char** argv) {

  FILE *g; int tam;
  int j;
  double * array;
  double media, varianza;

  /* double *tabla[3]; */
  double **tabla = malloc(3*sizeof(double*));

  if ( argc == 2 ) {
    g = fopen(argv[1], "r");
  } else {
    g = stdin;
  }

  for ( j = 0; j < 3; ++j ) {
    array = leer(g, &tam);
    media = calcular_media(array, tam);
    varianza = calcular_varianza(array, tam, media);
    printf("Media: %f, Varianza = %f\n", media, varianza);
    tabla[j] = array;
    /* free(array); Libero variable dinamica */
  }


  for ( j = 0; j < 3; ++j ) {
    free(tabla[j]);
  }
  free(tabla);

  /* Cierro el archivo */
  if ( argc == 2 ) {
    fclose(g);
  }

  return 0;
}

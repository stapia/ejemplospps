
#include <stddef.h> /* Para size_t */

double calcular_media(const double *array, size_t tam) {
	double media = 0;
	size_t i;
	for ( i = 0; i < tam; ++i ) {
		media += array[i];
	}
	media /= tam;
	return media;
}

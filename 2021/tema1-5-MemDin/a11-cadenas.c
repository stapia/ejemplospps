
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	int tam = 64;
	char *cadena = malloc(tam*sizeof(char));
	char token[32], aux[35];
	
	scanf("%31s", token);
	
	while ( strcmp(token, "fin") != 0 ) { // Mientras que token no sea fin
		// Hacemos algo con el token (concatenar)
		if ( strlen(cadena) + strlen(token) + 4 > tam ) {
			tam *= 2; // tam = 2 * tam;
			char *nueva_cadena = malloc(tam);
			// Copio lo que había antes
			strcpy(nueva_cadena, cadena);
			
			free(cadena); // Libero antiguo
			cadena = nueva_cadena;
		}
		sprintf(aux, "\"%s\";", token);
		strcat(cadena, aux);

		// Leemos un nuevo token
		scanf("%31s", token);
	}
	free(cadena);
	
	return 0;
}
